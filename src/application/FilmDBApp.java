package application;

import application.controllers.AppController;

/**
 * This class is the driver class. 
 * It contains the main method so that it can be found easily.
 * The films are downloaded from the Internet, queried and parsed from OMDB database.
 * 
 * @author Nikolai Kolbenev 15897074 */
public class FilmDBApp 
{
	/**
	 * Launch application
	 * 
	 * @param args - cmd args
	 * @author Nikolai Kolbenev 15897074
	 */
	public static void main(String[] args) 
	{
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FilmDBApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FilmDBApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FilmDBApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FilmDBApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		AppController app = new AppController("Film Database Manager");
		app.setVisible(true);
	}
}
