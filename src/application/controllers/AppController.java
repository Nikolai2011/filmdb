package application.controllers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import application.models.FilmReference;
import application.utilities.Layout;
import application.views.FilmSearchView;
import application.views.FilmView;
import application.views.AppView;
import application.views.MenuBarView;
import application.views.StatusView;

/**
 * This class forms the main window where all UI elements reside.
 * It aggregates all necessary functionality in the form of 3 controllers.
 * It contains several views with related groups of elements.
 * All other parts of this application are connected together via
 * this application controller object.
 * 
 * @author Nikolai Kolbenev 15897074
 */
public class AppController extends JFrame
{
	private FilmReference selectedFilmReferenceModel;
	
	private FilmDatabaseController filmDatabaseController;
	private FilmEditingController filmEditingController;
	private FilmSearchController filmSearchController;
	
	private MenuBarView menuBarView;
	private StatusView statusView;
	private AppView appView;
	private FilmView filmView;
	private FilmSearchView filmSearchView;
	
	private JComponent[] componentsWithChangingBorder;
	
	/**
	 * Creates a new instance of this AppController class.
	 * Controller classes pass models to views,
	 * so controllers should be created first
	 * 
	 * @param title The title to use for this window
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public AppController(String title)
	{
		super(title);
		
		this.setSize(1250, 600);
		this.setLocationRelativeTo(null); //Show window in center
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		selectedFilmReferenceModel = new FilmReference();
		filmDatabaseController = new FilmDatabaseController(this);
		filmEditingController = new FilmEditingController(this, selectedFilmReferenceModel);
		filmSearchController = new FilmSearchController(this);
		
		initComponents();
		loadComponents();
		setEventHandlers();
		
		filmView.update();
		appView.update();
	}
	
	/**
	 * Initialize views and UI elements and set their initial properties.
	 * Models are passed to views from controller classes.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	private void initComponents()
	{
		menuBarView = new MenuBarView();
		statusView = new StatusView();
		filmView = new FilmView(selectedFilmReferenceModel);
		filmSearchView = new FilmSearchView();
		appView = new AppView(filmDatabaseController.getFilmDBModel(), filmView, filmSearchView);
		componentsWithChangingBorder = new JComponent[] {filmView, filmSearchView};
		Layout.applyTwoColorGradient(componentsWithChangingBorder);
		Layout.applyTwoColorGradient(componentsWithChangingBorder);
		
		//This panel is only needed to layout GUI views in center of the window
		JPanel panelAggregator = new JPanel(new GridBagLayout());
		panelAggregator.add(appView);
		this.getContentPane().add(panelAggregator);
		
		appView.setPreferredSize(appView.calculatePreferredSize());
		appView.setMinimumSize(appView.getPreferredSize());
	}
	
	/**
	 * Place components on this window
	 * @author Nikolai Kolbenev 15897074
	 */
	private void loadComponents()
	{
		this.setJMenuBar(menuBarView);
		this.getContentPane().add(statusView, BorderLayout.SOUTH);
	}
	
	/**
	 * Assign listeners to events that are triggered by UI components.
	 * Event handlers are taken from controller classes.
	 * UI elements exist in views of this class.
	 * 
	 *  By the time this method is called, all controller classes and views
	 *  must be instantiated.
	 *  Lambda expressions are used to set handlers.
	 *  
	 *  @author Nikolai Kolbenev 15897074
	 */
	private void setEventHandlers()
	{
		this.addWindowListener(new WindowAdapter() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				filmDatabaseController.handleWindowOpening();
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				filmDatabaseController.handleWindowClosing();
			}
		});
		
		
		//Search on Enter press
		appView.getFilmSearchView().getTextFieldDirector().addActionListener((event) -> filmSearchController.handleSearchEvent());
		appView.getFilmSearchView().getTextFieldGenre().addActionListener((event) -> filmSearchController.handleSearchEvent());
		appView.getFilmSearchView().getTextFieldTitle().addActionListener((event) -> filmSearchController.handleSearchEvent());
		appView.getFilmSearchView().getTextAreaCastList().addKeyListener(new KeyAdapter() {
			@Override
		    public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					e.consume();
					filmSearchController.handleSearchEvent();
				}
			};
		});
		appView.getFilmSearchView().getTextFieldFromRuntime().addActionListener((event) -> filmSearchController.handleSearchEvent());
		appView.getFilmSearchView().getTextFieldToRuntime().addActionListener((event) -> filmSearchController.handleSearchEvent());
		
		appView.getButtonSearchFilm().addActionListener((event) -> filmSearchController.handleSearchEvent());
		appView.getButtonClearSearch().addActionListener((event) -> filmSearchController.handleClearFieldsButtonEvent());
		appView.getButtonShowAllFilms().addActionListener((event) -> filmSearchController.handleShowAllFilmsButtonEvent());
		
		appView.getComboBoxFilmOrdering().addActionListener((event) -> filmDatabaseController.handleComboBoxOrderingEvent());
		
		appView.getButtonNewFilm().addActionListener((event) -> filmEditingController.handleButtonNewFilmEvent());
		appView.getButtonAddFilm().addActionListener((event) -> filmDatabaseController.handleAddButtonEvent());
		appView.getButtonRemoveFilm().addActionListener((event) -> filmDatabaseController.handleRemoveButtonEvent());
		appView.getButtonSaveModifiedFilm().addActionListener((event) -> filmEditingController.handleSaveButtonEvent());
		appView.getJListFilms().addListSelectionListener((event) -> filmEditingController.handleFilmSelectionEvent());
		
		for (JRadioButton rButton: filmView.getRatingRadioButtons())
		{
			rButton.addActionListener((event) -> filmEditingController.handleRadioButtonEvent(event));
		}
		
		this.menuBarView.getMenuItemOpenDB().addActionListener((event) -> filmDatabaseController.handleMenuItemOpenDBEvent());
		this.menuBarView.getMenuItemSaveDB().addActionListener((event) -> filmDatabaseController.handleCurrentDBSaving());
		this.menuBarView.getMenuItemSaveAsDB().addActionListener((event) -> filmDatabaseController.handleMenuItemSaveAsDBEvent());
		this.menuBarView.getMenuItemRainbowBorder().addActionListener((event) -> Layout.applyRainbowGradient(componentsWithChangingBorder));
		this.menuBarView.getMenuItemTwoColorBorder().addActionListener((event) -> Layout.applyTwoColorGradient(componentsWithChangingBorder));
		this.menuBarView.getMenuItemDefaultBorder().addActionListener((event) -> Layout.applyDefaultBorder(componentsWithChangingBorder));
	}

	/**
	 * Film reference stores currently selected film
	 * @return The FilmReference object
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmReference getSelectedFilmReferenceModel() {
		return selectedFilmReferenceModel;
	}
	
	/**
	 * The statusView holds and displays database path
	 * 
	 * @return The StatusView object
	 * @author Nikolai Kolbenev 15897074
	 */
	public StatusView getStatusView() {
		return statusView;
	}

	
	/**
	 * This object contains all UI components of this application.
	 * 
	 * @return The AppView object
	 * @author Nikolai Kolbenev 15897074
	 */
	public AppView getAppView() {
		return appView;
	}

	/**
	 * The view with film data (title, genre, runtime, etc.)
	 * 
	 * @return The FilmView object
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmView getFilmView() {
		return filmView;
	}

	/**
	 * The view which helps input filtration data
	 * 
	 * @return The FilmSearchView object
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmSearchView getFilmSearchView() {
		return filmSearchView;
	}

	/**
	 * @return The FilmEditingController object
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmEditingController getFilmEditingController() {
		return filmEditingController;
	}

	/**
	 * @return The FilmSearchController object
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmSearchController getFilmSearchController() {
		return filmSearchController;
	}
	
	/**
	 * @return The FilmDatabaseController object
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmDatabaseController getFilmDatabaseController() {
		return filmDatabaseController;
	}
}
