package application.controllers;

import java.io.File;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashSet;
import java.util.TreeSet;

import javax.swing.JOptionPane;

import application.models.Film;
import application.models.FilmDatabase;
import application.utilities.FileManipulation;
import application.utilities.FilmDataManipulation;

/**
 * The database controller manipulates database data and
 * maintains a reference to the currently selected database file.
 * 
 * It also adds films to and removes them from the database.
 *
 * @author Nikolai Kolbenev 15897074
 */
public class FilmDatabaseController 
{
	private final AppController appController;
	
	private File currentDBFile;
	private FilmDatabase filmDBModel;
	
	/**
	 * Some data is initialized relating to DB functionality
	 * 
	 * @param appController The controller for this application
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmDatabaseController(AppController appController)
	{
		this.appController = appController;
		this.currentDBFile = new File(FileManipulation.DEFAULT_DATABASE_PATH);
		this.filmDBModel = new FilmDatabase();
		filmDBModel.addCollectionModifiedListener(() -> appController.getAppView().updateListData());
		filmDBModel.setFilmOrderingComparator(FilmDataManipulation.getFilmByTitleComparator()); //Initial comparator
	}
	
	/**
	 * Respond to window opening event and load database in default path.
	 * In case of failure, notification is provided and the user may
	 * continue to use this application.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleWindowOpening()
	{
		updateSelectedDBPath();
		try
		{
			HashSet<Film> films = FileManipulation.getFilmsFromFile(currentDBFile.getPath());
			if (films != null)
			{
				filmDBModel.setFilms(films);
			}
		}
		catch(Exception ex)
		{
			JOptionPane.showMessageDialog(appController, "Unable to locate and load the default database.\nError: " + ex.getMessage());
		}
	}

	/**
	 * Handle window closing. A standard dialog is created that
	 * prompts the user to save their database changes
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleWindowClosing()
	{
        if (suggestSavingAndContinue() == true)
        {
        	appController.dispose();
            System.exit(0);
        }
	}
	
	/**
	 * Respond to add film event.
	 * Data is read from text fields, parsed as a film object and GUI is updated.
	 * Films with the same title are prohibited.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleAddButtonEvent()
	{
		Film film = appController.getFilmEditingController().readFilmDataFields();
		if (film != null)
		{
			if (filmDBModel.contains(film))
			{
				JOptionPane.showMessageDialog(appController, "A film with the same title already exists.");
			}
			else
			{
				filmDBModel.add(film);
				appController.getAppView().getJListFilms().setSelectedValue(film, true);
				appController.getAppView().updateButtonStates();
				appController.getAppView().displayNotification("A new film has been added!");;
			}
		}
	}

	/**
	 * Respond to remove film event.
	 * The currently selected film is removed from the database.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleRemoveButtonEvent()
	{
		if (appController.getAppView().getJListFilms().getSelectedIndices().length == 1)
		{
			Film selectedFilm = appController.getAppView().getJListFilms().getSelectedValue();
			boolean wasRemoved = filmDBModel.remove(selectedFilm);
			if (wasRemoved)
			{
				appController.getAppView().displayNotification("The film has been removed.");
			}
			else
			{
				appController.getAppView().displayNotification("Cannot remove the film!");
			}
		}
	}
	
	/**
	 * Respond to database save event.
	 * Attempts to write film data to the current database file and
	 * notifies about success.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleCurrentDBSaving()
	{
		boolean wasSaved = FileManipulation.saveFilmsToFile(currentDBFile.getPath(), filmDBModel, appController);
		if (wasSaved)
		{
			JOptionPane.showMessageDialog(appController, "Current database has been saved.");
		}
	}
	
	/**
	 * Respond to open database event. A prompt to save changes is displayed first.
	 * Then user browses the database directory and chooses which one to import.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleMenuItemOpenDBEvent()
	{
		if (!suggestSavingAndContinue())
		{
			return;
		}

		File selectedFile = FileManipulation.browseFilmDBDirectory(appController, true);
		if (selectedFile != null)
		{
			try
			{
				HashSet<Film> films = FileManipulation.getFilmsFromFile(selectedFile.getPath());
				if (films != null)
				{
					filmDBModel.setFilms(films);
					appController.getFilmView().clearFields();

					this.currentDBFile = selectedFile;
					updateSelectedDBPath();
				}
			}
			catch (Exception ex)
			{
				JOptionPane.showMessageDialog(appController, "Cannot import films from file.\nOriginal error: " + ex.getMessage());
			}
		}
	}

	/**
	 * Respond to "save as" event. The user is able to browse file system
	 * and choose where to save the database.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleMenuItemSaveAsDBEvent()
	{
		File selectedFile = FileManipulation.browseFilmDBDirectory(appController, false);
		if (selectedFile != null)
		{
			FileManipulation.saveFilmsToFile(selectedFile.getPath(), filmDBModel, appController);
		}
	}
	
	/**
	 * Respond to sort ordering changed. Films are to be sorted in a specified order, 
	 * which is indicated by the combobox. Several sort options are provided and the 
	 * rules for sorting are defined by appropriate comparator stored in the database object.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleComboBoxOrderingEvent()
	{
		String order = (String)appController.getAppView().getComboBoxFilmOrdering().getSelectedItem();
		Comparator<Film> comparator = this.filmDBModel.getFilmOrderingComparator();
		switch(order)
		{
		case "By Title":
			if (comparator == FilmDataManipulation.getFilmByTitleComparator())
			{
				this.filmDBModel.setFilmOrderingComparator(comparator.reversed());
			}
			else
			{
				this.filmDBModel.setFilmOrderingComparator(FilmDataManipulation.getFilmByTitleComparator());
			}
			break;
		case "By Year":
			if (comparator == FilmDataManipulation.getFilmByYearComparator())
			{
				this.filmDBModel.setFilmOrderingComparator(comparator.reversed());
			}
			else
			{
				this.filmDBModel.setFilmOrderingComparator(FilmDataManipulation.getFilmByYearComparator());
			}
			break;
		case "By Rating":
			if (comparator == FilmDataManipulation.getFilmByRatingComparator()) {
				this.filmDBModel.setFilmOrderingComparator(comparator.reversed());
			} else {
				this.filmDBModel.setFilmOrderingComparator(FilmDataManipulation.getFilmByRatingComparator());
			}
			break;
		case "By Runtime":
			if (comparator == FilmDataManipulation.getFilmByRuntimeComparator())
			{
				this.filmDBModel.setFilmOrderingComparator(comparator.reversed());
			}
			else
			{
				this.filmDBModel.setFilmOrderingComparator(FilmDataManipulation.getFilmByRuntimeComparator());
			}
			break;
		}
		
		appController.getAppView().updateListData();
	}
	
	/**
	 * Update DB path shown by label component.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void updateSelectedDBPath()
	{
		appController.getStatusView().getLabelFilePath().setText("Database File Name: " + "\"" + Paths.get(currentDBFile.getPath()).getFileName() + "\"");
	}
	
	/**
	 * Provide interaction with the user in which they may choose to save 
	 * changes, move on or go back by choosing a "cancel" option. This method helps
	 * ensure that execution of the next scheduled operation (such as moving away from current database or closing the window) 
	 * will not affect the changes made to the database.
	 * 
	 * @return true if the user is ready to move on or if all changes have
	 * been saved successfully, otherwise false  
	 * @author Nikolai Kolbenev 15897074
	 */
	public boolean suggestSavingAndContinue()
    {
		boolean moveOn = false;

        if (currentDBFile != null)
        {
        	int result = JOptionPane.showConfirmDialog(appController, "Any unsaved changes will be lost.\n\nWould you like to save your current database?\n\n", "Save", JOptionPane.YES_NO_CANCEL_OPTION);
            
            switch (result)
            {
                case JOptionPane.YES_OPTION:
                    boolean wasSaved = FileManipulation.saveFilmsToFile(currentDBFile.getPath(), filmDBModel, appController);
                    moveOn = wasSaved;
                    break;
                case JOptionPane.NO_OPTION:
                    moveOn = true;
                    break;
                default:
                    moveOn = false;
                    break;
            }
        }
        else
        {
            moveOn = true;
        }

        return moveOn;
    }
	
	/**
	 * @return File object with the path of current database
	 * @author Nikolai Kolbenev 15897074
	 */
	public File getCurrentDBFile() {
		return currentDBFile;
	}

	/**
	 * @return FilmDatabase object with films
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmDatabase getFilmDBModel() {
		return filmDBModel;
	}
}
