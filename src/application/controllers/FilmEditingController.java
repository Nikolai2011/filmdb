package application.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.nio.file.Paths;
import java.util.TreeSet;

import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.event.ListSelectionEvent;

import application.models.Film;
import application.models.FilmDatabase;
import application.models.FilmReference;
import application.utilities.FilmDataManipulation;
import application.utilities.FileManipulation;
import application.utilities.FilmTextData;

/**
 * The film editing controller encapsulates methods that change film
 * properties and update the GUI. It keeps track of currently selected
 * film and displays its data.
 * 
 * This class is also responsible for reading film data and converting it
 * to the real object.
 * 
 * The controller may be put into search mode when no editing is allowed.
 *
 * @author Nikolai Kolbenev 15897074
 */
public class FilmEditingController
{
	private final AppController appController;
	
	private final FilmReference selectedFilmReferenceModel;
	private boolean isInSearchMode = false;
	
	/**
	 * The Constructor maintains a reference to the reference
	 * to a selected film object. 
	 * I need to be able to point to a new film object and make anyone who used
	 * that object to use a new reference instead.
	 * 
	 * @param appController The controller for this application
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmEditingController(AppController appController, FilmReference selectedFilmeReference)
	{
		this.appController = appController;
		this.selectedFilmReferenceModel = selectedFilmeReference;
	}
	
	/**
	 * Respond to new film request
	 * 
	 * The method prepares the view and clears all fields
	 * to allow the user to enter new film data
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleButtonNewFilmEvent()
	{
		appController.getFilmView().clearFields();
		appController.getAppView().getJListFilms().clearSelection();
		appController.getAppView().updateButtonStates();
		appController.getFilmView().getFilmMainPropView().getTextFieldTitle().requestFocusInWindow(); //Put focus on "Title" field
	}
	
	/**
	 * Respond to save film modification event.
	 * 
	 * Ensure that only valid changes will be applied to the currently selected film.
	 * Updates GUI so that film can get new position in the list (if title changes) 
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleSaveButtonEvent()
	{
		if (selectedFilmReferenceModel.getFilm() != null)
		{
			Film updatedFilm = readFilmDataFields();
			if (updatedFilm != null)
			{
				appController.getFilmDatabaseController().getFilmDBModel().remove(selectedFilmReferenceModel.getFilm());
				appController.getFilmDatabaseController().getFilmDBModel().add(updatedFilm);

				selectedFilmReferenceModel.setFilm(updatedFilm); //Restore previously selected film after selection loss
				appController.getAppView().getJListFilms().setSelectedValue(selectedFilmReferenceModel.getFilm(), true);
				appController.getAppView().updateButtonStates();

				appController.getAppView().displayNotification("Film has been modified. To keep changes save the database as well.");
			}
		}
		else
		{
			JOptionPane.showMessageDialog(appController, "You must first select a film.");
		}
	}
	
	/**
	 * This method just moves selection from one radio button to
	 * the other and ensures that only one radio button is selected
	 * 
	 * @param event The ActionEvent object associated with a button
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleRadioButtonEvent(ActionEvent event)
	{
		if (event.getSource() instanceof JRadioButton)
		{
			JRadioButton element = (JRadioButton)event.getSource();

			for (JRadioButton rButton: appController.getFilmView().getRatingRadioButtons())
			{
				rButton.setSelected(false);
			}

			element.setSelected(true);
		}
	}

	/**
	 * Respond to film selection event.
	 * 
	 * Display all relevant information about the film. Button states are 
	 * changed only when film editing is enabled (i.e. in non-search mode)
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleFilmSelectionEvent()
	{
		selectedFilmReferenceModel.setFilm(appController.getAppView().getJListFilms().getSelectedValue());
		appController.getFilmView().update();
		if (!isInSearchMode)
		{
			appController.getAppView().updateButtonStates();
		}
	}
	
	/**
	 * Attempt to read film data from user interface and 
	 * create a film object from it. A failure message is provided
	 * 
	 * @return The Film object with initialized data
	 * @author Nikolai Kolbenev 15897074
	 */
	public Film readFilmDataFields()
	{
		Film film = null;
		FilmTextData filmData = new FilmTextData();
		
		filmData.setTitle(appController.getFilmView().getFilmMainPropView().getTextFieldTitle().getText().trim());
		filmData.setDirector(appController.getFilmView().getFilmMainPropView().getTextFieldDirector().getText().trim());
		filmData.setGenre(appController.getFilmView().getFilmMainPropView().getTextFieldGenre().getText().trim());
		filmData.setCastList(appController.getFilmView().getFilmMainPropView().getTextAreaCastList().getText().trim());
		filmData.setRating(String.valueOf(appController.getFilmView().getSelectedRating()));
		filmData.setReleaseYear(String.valueOf(appController.getFilmView().getComboBoxReleaseYear().getSelectedItem()));
		filmData.setRuntime(appController.getFilmView().getTextFieldRuntime().getText());
		
		String invalidFields = FilmDataManipulation.getParseFilmDataResult(filmData);
		if (!invalidFields.isEmpty())
		{
			JOptionPane.showMessageDialog(appController, "Invalid data: \n\n" + invalidFields + "\n");
		}
		else
		{
			try
			{
				film = new Film(filmData);
			}
			catch(NumberFormatException ex)
			{
				JOptionPane.showMessageDialog(appController, "Cannot read data, unexpected.\n\n");
			};
		}
		
		return film;
	}

	/**
	 * In search mode all film editing buttons are hidden so that changes
	 *  cannot be made to search results. In theory, all changes will be reflected
	 *  on the original list of films so the save button may remain visible. But I also
	 *  have the add button that will not work in this case.
	 * 
	 * @param isInSearchMode The boolean value to set search mode
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setIsInSearchMode(boolean isInSearchMode)
	{
		this.isInSearchMode = isInSearchMode;
		
		if (isInSearchMode)
		{
			appController.getAppView().setFilmViewButtonsVisibility(false);
			appController.getAppView().getLabelSearchModeTip().setVisible(true);
		}
		else
		{
			appController.getAppView().updateButtonStates();
			appController.getAppView().getLabelSearchModeTip().setVisible(false);
		}
	}
}
