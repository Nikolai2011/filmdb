package application.controllers;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.JOptionPane;
import application.models.Film;
import application.models.FilmDatabase;

/**
 * This controller provides film search functionality.
 * 
 * It works closely with FilmSearchView view, retrieving film search data from UI components
 * and processing it.
 * 
 * At some point it needs to ask FilmDatabaseController for current database to search and
 * filter, also asking for which database file is currently used.
 * 
 * @author Nikolai Kolbenev 15897074
 */
public class FilmSearchController 
{
	private AppController appController;
	
	private File cachedCurrentDBFile;
	private HashSet<Film> cachedFilms;
	
	/**
	 * Create an instance of FilmSearchController class
	 * 
	 * @param appController The AppController object
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmSearchController(AppController appController)
	{
		this.appController = appController;
	}

	/**
	 * Exit search mode and show edit buttons that allow to
	 * modify film database. On exit, all films are loaded from the database adn
	 * any filtered results are removed.
	 * 
	 * The search mode applies to FilmEditingController to disable film editing
	 * capability. Because adding and removing films from the list of search results has
	 * no effect on the original database, the search mode helps avoid confusion.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void exitSearchMode()
	{
		updateCachedData();
		appController.getFilmEditingController().setIsInSearchMode(false);
		if (cachedFilms != null)
		{
			appController.getFilmDatabaseController().getFilmDBModel().setFilms(cachedFilms);
			cachedFilms = null;
		}
	}
	
	/**
	 * Responds to button press event that should show all films in the database,
	 * also enabling to edit them. 
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleShowAllFilmsButtonEvent()
	{
		exitSearchMode();
	}

	/**
	 * Responds to the button event that initiates searching process.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleSearchEvent()
	{
		initSearchStep1();
	}
	
	/**
	 * Responds to the button event that should clear all fields in search view
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void handleClearFieldsButtonEvent()
	{
		appController.getFilmSearchView().clearFields();
	}

	/**
	 * This method prepares half of the data to be used as film criteria.
	 * It retrieves the data from corresponding views, all of which are accessible from 
	 * one point, namely the appController object.
	 * It checks the film "runtime" value as it is the only value that may get invalid input
	 * from the user(In a text field). It then swaps integers to determine max and min, if necessary.
	 * 
	 * Next, it passes integer values to beginSearchFilms method. The reasons to pass
	 * them as arguments are explained in that method.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	private void initSearchStep1()
	{
		int minRating = (Integer)appController.getFilmSearchView().getComboBoxFromRating().getSelectedItem();
		int maxRating = (Integer)appController.getFilmSearchView().getComboBoxToRating().getSelectedItem();
		int minYear = (Integer)appController.getFilmSearchView().getComboBoxFromYear().getSelectedItem();
		int maxYear = (Integer)appController.getFilmSearchView().getComboBoxToYear().getSelectedItem();
		int minRuntime = Integer.MIN_VALUE;
		int maxRuntime = Integer.MAX_VALUE;
		
		try
		{
			minRuntime =  Integer.valueOf(appController.getFilmSearchView().getTextFieldFromRuntime().getText());
			maxRuntime =  Integer.valueOf(appController.getFilmSearchView().getTextFieldToRuntime().getText());
		}
		catch(NumberFormatException ex)
		{
			JOptionPane.showMessageDialog(appController, "Invalid data in search view: \n\n\"Runtime\" value must be a number.\n");
			return;
		}
		
		if (minRuntime > maxRuntime)
		{
			int minTemp = minRuntime;
			minRuntime = maxRuntime;
			maxRuntime = minTemp;
		}

		if (minRating > maxRating)
		{
			int minTemp = minRating;
			minRating = maxRating;
			maxRating = minTemp;
		}
		
		if (minYear > maxYear)
		{
			int minTemp = minYear;
			minYear = maxYear;
			maxYear = minTemp;
		}
		
		initSearchStep2(minRuntime, maxRuntime, minRating, maxRating, minYear, maxYear);
	}
	
	/**
	 * Prepares the second half of the data for film searching.
	 * When done, this method checks that we have a valid reference to 
	 * the original database, if necessary it updates the reference.
	 * 
	 * In this method I use a Predicate object to filter films.
	 * I am passing only integer values as parameters. This way it doesn't produce any compilation errors
	 * that require to declare final on integers that are passed to predicate. 
	 * Not sure why String values can be declared locally, probably because they are immutable anyway.
	 * 
	 * @param minRuntime The minimum runtime boundary
	 * @param maxRuntime The maximum runtime boundary
	 * @param minRating The min rating boundary
	 * @param maxRating The max rating boundary
	 * @param minYear The min year boundary
	 * @param maxYear The max year boundary
	 * @author Nikolai Kolbenev 15897074
	 */
	private void initSearchStep2(int minRuntime, int maxRuntime, int minRating, int maxRating, int minYear, int maxYear)
	{
		try
		{
			String title = appController.getFilmSearchView().getTextFieldTitle().getText();
			String director = appController.getFilmSearchView().getTextFieldDirector().getText();
			String genre = appController.getFilmSearchView().getTextFieldGenre().getText().trim();
			String castList = appController.getFilmSearchView().getTextAreaCastList().getText().trim();
			
			boolean isRegexEnabled = appController.getFilmSearchView().getCheckBoxRegex().isSelected();
			if (isRegexEnabled)
			{
				//Check that all regular expressions are valid
				try
				{
					Pattern.compile(title);
					Pattern.compile(director);
					Pattern.compile(genre);
					Pattern.compile(castList);
				}
				catch(PatternSyntaxException ex)
				{
					JOptionPane.showMessageDialog(appController, "The regex you specified is not valid.\n\nOriginal error: " + ex.getMessage());
					return;
				};
			}
			
			//Filtration rules
			Predicate<Film> predicate;
			if (isRegexEnabled)
			{
				//All fields (title, director, castList...) are treated as fields
				//containing regular expressions. Empty regexes are not processed (due to || operator)
				predicate = ((film) -> (
						film.getReleaseYear() >= minYear && film.getReleaseYear() <= maxYear &&
						film.getRating() >= minRating && film.getRating() <= maxRating &&
						film.getRuntime() >= minRuntime && film.getRuntime() <= maxRuntime &&
						(title.isEmpty() || film.getTitle().matches(title)) && 
						(director.isEmpty() || film.getDirector().matches(director)) &&
						(genre.isEmpty() || film.getGenre().matches(genre)) && 
						(castList.isEmpty() || film.getCastList().matches(castList))
						));
			}
			else
			{
				predicate = ((film) -> (
						film.getReleaseYear() >= minYear && film.getReleaseYear() <= maxYear &&
						film.getRating() >= minRating && film.getRating() <= maxRating &&
						film.getRuntime() >= minRuntime && film.getRuntime() <= maxRuntime &&
						film.getTitle().trim().toLowerCase().contains(title.trim().toLowerCase()) && 
						film.getDirector().trim().toLowerCase().contains(director.trim().toLowerCase()) &&
						film.getGenre().trim().toLowerCase().contains(genre.trim().toLowerCase()) && 
						film.getCastList().trim().toLowerCase().contains(castList.trim().toLowerCase())
						));
			}
			
			//This actually prevents cachedFilms from being assigned the search results.
			//Search results are written to filmDBModel object and we need to make sure that cache only
			//references the original film database.
			updateCachedData();
			if (cachedFilms == null)
			{
				this.cachedFilms = appController.getFilmDatabaseController().getFilmDBModel().getFilms();
			}
			
			//Begin search call
			beginSearch(predicate);
		}
		catch(Exception ex){};
	}

	
	/**
	 * This method searches the cached film database and assigns results to the
	 * existing film database reference, which is used by the other controller classes, 
	 * This action auto updates relevant GUI (via event listener attached to 
	 * film database internal changes)
	 * 
	 * @param predicate The film filtration policy
	 * @author Nikolai Kolbenev 15897074
	 */
	private void beginSearch(Predicate<Film> predicate)
	{
		Film[] searchResults = cachedFilms.stream().filter(predicate).toArray((size) -> new Film[size]);

		FilmDatabase filmDB = appController.getFilmDatabaseController().getFilmDBModel();
		filmDB.setFilms(new HashSet<Film>(Arrays.asList(searchResults)));
		
		//Finally, we can enter search mode that does nothing more than
		//just hide edit buttons :)
		appController.getFilmEditingController().setIsInSearchMode(true);

		System.out.println(searchResults.length + " films found!");
	}
	
	/**
	 * Auto-detects transfer to a new database from a new file and
	 * removes cached database from the previous file so that it is no
	 * longer available
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	private void updateCachedData()
	{
		if (cachedCurrentDBFile != appController.getFilmDatabaseController().getCurrentDBFile())
		{
			cachedCurrentDBFile = appController.getFilmDatabaseController().getCurrentDBFile();
			cachedFilms = null;
		}
	}
}
