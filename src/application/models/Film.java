package application.models;

import application.utilities.FilmTextData;

/**
 * The film class that represents data about a real film.
 * All films are compared by their title. Identical titles are not allowed in the list.
 * 
 * @author Nikolai Kolbenev 15897074
 */
public class Film implements Comparable<Film>
{
	private String title;
	private String genre;
	private String director;
	private String castList;
	private int rating;
	private int releaseYear;
	private int runtime;
	
	/**
	 * This constructor enables to accept a single 
	 * parameter with possibly valid data
	 * 
	 * @param filmData FilmTextData object that contains text
	 * representation of the film's fields.
	 * @throws NumberFormatException exception indicating that string data is not numeric
	 * @author Nikolai Kolbenev 15897074
	 */
	public Film(FilmTextData filmData) throws NumberFormatException
	{
		super();
		setFields(filmData.getTitle(), filmData.getDirector(), filmData.getGenre(), filmData.getCastList(), 
				Integer.valueOf(filmData.getRating()), Integer.valueOf(filmData.getReleaseYear()), Integer.valueOf(filmData.getRuntime()));
	}
	
	/**
	 * This constructor ensures that data is valid and has corresponding types
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public Film(String title, String director, String genre, String castList,
			int rating,int releaseYear, int runtime) 
	{
		super();
		setFields(title, director, genre, castList, rating, releaseYear, runtime);
	}
	
	
	/**
	 * @param title The title of this film in String 
	 * @param director The director of this film in String
	 * @param genre The genre of this film in String
	 * @param castList The cast list of this film in String
	 * @param rating The title of this film in an integer value
	 * @param releaseYear The release year of this film in an integer value 
	 * @param runtime The runtime of this film in an integer value 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setFields(String title, String director, String genre, String castList,
			int rating, int releaseYear, int runtime)
	{
		setTitle(title);
		setDirector(director);
		setRating(rating);
		setReleaseYear(releaseYear);
		setCastList(castList);
		setGenre(genre);
		setRuntime(runtime);
	}
	
	/**
	 * This method is necessary for displaying films
	 * by their title names in jList.
	 * 
	 * @return The title of this film
	 * @author Nikolai Kolbenev 15897074
	 */
	@Override
	public String toString()
	{
		return title;
	}

	@Override
	public boolean equals(Object film)
	{
		if (film instanceof Film == false)
		{
			return false;
		}
		else
		{
			return getTitle().trim().toLowerCase().equals(((Film)film).getTitle().trim().toLowerCase());
		}
	}
	
	@Override
	public int hashCode() {
		return getTitle().trim().toLowerCase().hashCode();
	};
	
	/** 
	 *     (OLD COMMENT)
	 * The method is used by TreeSet to sort trees
	 * 
	 * @param film The film object to compare to
	 * @return The position of this film object relative
	 * to the specified film
	 * @author Nikolai Kolbenev 15897074
	 */
	@Override
	public int compareTo(Film film) {
		return getTitle().trim().toLowerCase().compareTo(film.getTitle().trim().toLowerCase());
	}

	/**
	 * @return String of title
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title The String of title to set
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setTitle(String title) {
		if (title != null)
			this.title = title;
	}

	/**
	 * @return String of director
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getDirector() {
		return director;
	}

	/**
	 * @param director The String of director to set
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setDirector(String director) {
		if(director != null)
			this.director = director;
	}

	/**
	 * @return An integer with film rating 
	 * @author Nikolai Kolbenev 15897074
	 */
	public int getRating() {
		return rating;
	}

	/**
	 * @param rating The rating as an integer
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}

	/**
	 * @return Integer as a release year
	 * @author Nikolai Kolbenev 15897074
	 */
	public int getReleaseYear() {
		return releaseYear;
	}

	/**
	 * @param releaseYear The release year in integer
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setReleaseYear(int releaseYear) {
		this.releaseYear = releaseYear;
	}

	/**
	 * @return String of cast list
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getCastList() {
		return castList;
	}

	/**
	 * @param castList The cast list in String
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setCastList(String castList) {
		if (castList != null)
			this.castList = castList;
	}

	/**
	 * @return String of genre
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * @param genre The genre in String
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setGenre(String genre) {
		if (genre != null)
			this.genre = genre;
	}

	/**
	 * @return Runtime as an integer
	 * @author Nikolai Kolbenev 15897074
	 */
	public int getRuntime() {
		if (runtime < 0)
		{
			runtime = 0;
		}
		return runtime;
	}

	/**
	 * @param runtime Integer as a film runtime
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setRuntime(int runtime) {
		if (runtime >= 0)
		{
			this.runtime = runtime;	
		}
	}
}
