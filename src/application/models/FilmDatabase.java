package application.models;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Optional;
import java.util.TreeSet;

import application.utilities.Callback;
import application.utilities.FilmDataManipulation;

/**
 *    (OLD COMMENT)
 * The film database class that stores films in a TreeSet structure.
 * TreeSet does not allow duplicate values and it is preferred to an ArrayList structure.
 * Unlike HashSet, TreeSet prohibits null values as its elements, so it is 
 * easier to work with and introduces less bugs. TreeSet also keeps films initially 
 * sorted according to their titles. Unlike HashSet, TreeSet does not calculate 
 * hash code of its elements to locate them because they are already sorted and
 * easily found. The only method it uses is "compareTo" of Comparable interface.
 *
 * The comparator is used to sort films not only by titles but by release year and other
 * parameters as well. Title sorting is the default.
 * 
 * I introduced event handling to notify listeners when film collection is changed, so that
 * all necessary components can modify their visual state in a timely fashion. 
 * 
 * @author Nikolai Kolbenev 15897074
 */
public class FilmDatabase 
{
	private Comparator<Film> filmOrderingComparator;
	private HashSet<Film> films;
	private ArrayList<Callback> eventListeners = new ArrayList<Callback>();
	
	/**
	 * Create new instance of DB
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmDatabase()
	{
		this(new HashSet<Film>());
	}
	
	
	/**
	 * @param films TreeSet object with existing films
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmDatabase(HashSet<Film> films)
	{
		if (films == null)
		{
			throw new NullPointerException();
		}
		else
		{
			this.films = films;
		}
	}
	
	/**
	 * @param listener The event handler that will be notified/called with no parameters
	 * @author Nikolai Kolbenev 15897074
	 */
	public void addCollectionModifiedListener(Callback listener)
	{
		eventListeners.add(listener);
	}
	
	/**
	 * Notify/call all event handlers that subscribed to collection changed event 
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	private void notifyCollectionChanged()
	{
		for (Callback listener: eventListeners)
		{
			listener.call();
		}
	}
	
	/**
	 * @param film The film to check
	 * @return true if there is already the specified film in the database, otherwise false
	 * @author Nikolai Kolbenev 15897074
	 */
	public boolean contains(Film film)
	{
		return films.contains(film);
	}
	
	/**
	 * @param film The Film object to add
	 * @author Nikolai Kolbenev 15897074
	 */
	public void add(Film film)
	{
		if (film != null)
		{
			films.add(film);
			notifyCollectionChanged();
		}
	}

	/**
	 * @param film The FIlm object to remove
	 * @author Nikolai Kolbenev 15897074
	 */
	public boolean remove(Film film)
	{		
		boolean removed = false;
		if (film != null)
		{
			removed = films.remove(film);
		}
		if (removed)
		{
			notifyCollectionChanged();	
		}
		
		return removed;
	}

	/**
	 * @return TreeSet collection with films
	 * @author Nikolai Kolbenev 15897074
	 */
	public HashSet<Film> getFilms() 
	{
		return films;
	}

	/**
	 * @param films TreeSet collection with films
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setFilms(HashSet<Film> films) 
	{
		if (this.films != films)
		{
			this.films = films;
			notifyCollectionChanged();
		}
	}

	/**
	 * The array is formed from TreeSet collection.
	 * It is sorted by a comparator.
	 * 
	 * @return An array of Film objects
	 * @author Nikolai Kolbenev 15897074
	 */
	public Film[] toList()
	{
		Film[] list = films.toArray(new Film[films.size()]);
		
		Arrays.sort(list, FilmDataManipulation.getFilmByTitleComparator());
		if (this.filmOrderingComparator != null)
		{
			Arrays.sort(list, filmOrderingComparator);
		}
		
		return list;
	}

	/**
	 * @param filmOrderingComparator The comparator for sorting the films
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setFilmOrderingComparator(Comparator<Film> filmOrderingComparator) {
		this.filmOrderingComparator = filmOrderingComparator;
	}


	public Comparator<Film> getFilmOrderingComparator() {
		return filmOrderingComparator;
	}
}
