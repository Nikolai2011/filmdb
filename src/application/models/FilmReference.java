package application.models;

/**
 * A reference to the Film reference of a selected film object. 
 * This film reference is shared with film view and film editing controller.
 * I need to be able to point to a newly selected film by the "Film film" reference
 * so that others can see the same selected film.
 *
 *@author Nikolai Kolbenev 15897074
 */
public class FilmReference 
{
	private Film film; //The reference to a film object

	/**
	 * Get the reference to a Film object
	 * 
	 * @return The reference to a Film object
	 * @author Nikolai Kolbenev 15897074
	 */
	public Film getFilm() {
		return film;
	}

	/**
	 * Set the reference to a Film object
	 * 
	 * @param film The reference to a Film object
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setFilm(Film film) {
		this.film = film;
	}
}
