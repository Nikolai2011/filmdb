package application.utilities;

/**
 * The interface to allow event subscriber to receive 
 * notification from the event source
 *
 * @author Nikolai Kolbenev 15897074
 */
public interface Callback 
{
	/**
	 * Call/notify. Used to notify every listener about
	 * film database modification. Then it's up to the listener
	 * to execute appropriate methods and update the GUI.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void call();
}
