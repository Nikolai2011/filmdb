package application.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.TreeSet;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import application.controllers.AppController;
import application.models.Film;
import application.models.FilmDatabase;

/**
 * The helper class to work with files and check data integrity.
 * The import/export films functionality is provided.
 * The default database path is defined here, the database was populated with films via
 * a query to OMDB film database, a free web service to obtain movie information 
 * (I downloaded titles of most popular films from the Internet and sent them to OMDB one at a time to get a json string data).
 *
 * @author Nikolai Kolbenev 15897074
 */
public class FileManipulation 
{
	public static final String FILM_END_HARD_QUESSABLE_KEY = ")~@UNIQUE_END_ID&)#_+@";
	public static final String DEFAULT_DATABASE_PATH = "bin" + File.separator + "data" + File.separator + "AllFilms.txt";
	public static final String FILE_ENCODING_STRING = "UTF-8";
	
	private static JFileChooser fileChooser = new JFileChooser(DEFAULT_DATABASE_PATH);
	
	/**
	 * @param filePath The path as a String along which all directories will be created
	 * @author Nikolai Kolbenev 15897074
	 */
	public static void buildPath(String filePath)
	{
		File file = new File(filePath);
		file.getParentFile().mkdirs();
	}
	
	/**
	 * @param filePath The path as a String to write films to
	 * @param filmDB A database with films that need saving
	 * @param dbOwner The owner of both the database and the message dialog
	 * @return true if saving was successful, otherwise false
	 * @author Nikolai Kolbenev 15897074
	 */
	public static boolean saveFilmsToFile(String filePath, FilmDatabase filmDB, AppController dbOwner)
	{
		dbOwner.getFilmSearchController().exitSearchMode();
		boolean wasSaved = true;
		buildPath(filePath);
		//Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), FILE_ENCODING_STRING))
		//Writer writer = Files.newBufferedWriter(Paths.get(filePath), Charset.forName(FILE_ENCODING_STRING))
		Film[] films = filmDB.toList();
		try (Writer writer = Files.newBufferedWriter(Paths.get(filePath), Charset.forName(FILE_ENCODING_STRING))) 
		{
			for (Film film: films)
			{
				String packedData = FilmDataManipulation.packFilmData(film);
				writer.write(packedData + System.lineSeparator());
			}
		}
		catch(Exception ex)
		{
			wasSaved = false;
			JOptionPane.showMessageDialog(dbOwner, "Failed to save the project: \n\n" + ex);
		};

		return wasSaved;
	}
	
	/**
	 * @param filePath The path as a String from which films are exported
	 * @return A TreeSet structure with loaded films
	 * @throws Exception An exception if something bad happens
	 * @author Nikolai Kolbenev 15897074
	 */
	public static HashSet<Film> getFilmsFromFile(String filePath) throws Exception
	{
		HashSet<Film> films = null;
		buildPath(filePath);
		//BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), FILE_ENCODING_STRING))
		//BufferedReader reader = Files.newBufferedReader(Paths.get(filePath), Charset.forName(FILE_ENCODING_STRING))
		try (BufferedReader reader = Files.newBufferedReader(Paths.get(filePath), Charset.forName(FILE_ENCODING_STRING)))
		{
			films = new HashSet<Film>();
			
			String filmData = "";
			int filmCount = 0;
			String attribute = reader.readLine();
			while (attribute != null)
			{
				filmData += attribute + System.lineSeparator();

				if (attribute.equals(FILM_END_HARD_QUESSABLE_KEY))
				{
					try
					{
						Film unpackedFilm = FilmDataManipulation.unpackFilmData(filmData);
						films.add(unpackedFilm);
						filmCount++;
					}
					catch (Exception ex)
					{
						System.err.println("Film not loaded: " + ex.getMessage());
					};
					
					filmData = "";
				}
				
				attribute = reader.readLine();
			}

			
			System.out.println(filmCount + " Films in the database!");
		} 
		catch (Exception e) 
		{
			throw e;
		}
		
		return films;
	}

	/**
	 * @param dbOwner The owner of the browse dialog
	 * @param showOpenDialog true to show open dialog, false to show save dialog
	 * @return A reference to the file that was selected
	 * @author Nikolai Kolbenev 15897074
	 */
	public static File browseFilmDBDirectory(AppController dbOwner, boolean showOpenDialog)
	{
		dbOwner.getFilmSearchController().exitSearchMode();
		buildPath(DEFAULT_DATABASE_PATH);
		fileChooser.setCurrentDirectory(Paths.get(DEFAULT_DATABASE_PATH).getParent().toFile());
		
		File selectedFile = null;
		int returnVal = (showOpenDialog) ? fileChooser.showOpenDialog(dbOwner) : fileChooser.showSaveDialog(dbOwner);
		
        if (returnVal == JFileChooser.APPROVE_OPTION) 
        {
            selectedFile = fileChooser.getSelectedFile();
        } 
        
        return selectedFile;
	}
}
