package application.utilities;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import application.models.Film;

/**
 * The film data processing is done by this class.
 * The film data can be packed to a string in this class
 * and then written to a file by the FileManipulation class. 
 * The string is formatted in a certain way before being written to a file.
 * Optionally, a text representation of Film data may be checked by this class as well.
 * 
 * @author Nikolai Kolbenev 15897074
 */
public class FilmDataManipulation 
{
	public static final int MAX_RELEASE_YEAR = LocalDate.now().getYear() + 10;
	public static final int MIN_RELEASE_YEAR = 1900;
	
	/**
	 * @return An array of integer values that represent all possible values
	 * for film release years.
	 * @author Nikolai Kolbenev 15897074
	 */
	public static Integer[] getReleaseYearValues()
	{
		Integer[] values = new Integer[MAX_RELEASE_YEAR - MIN_RELEASE_YEAR + 1];
		
		int year = MIN_RELEASE_YEAR;
		for (int i = 0; i < values.length; i++)
		{
			values[i] = year++;
		}

		Arrays.sort(values, Collections.reverseOrder());
		return values;
	}
	
	public static Comparator<Film> getFilmByTitleComparator()
	{
		return (filmLeft, filmRight) -> filmLeft.getTitle().compareTo(filmRight.getTitle());
	}
	
	public static Comparator<Film> getFilmByYearComparator()
	{
		return (filmLeft, filmRight) -> Integer.compare(filmRight.getReleaseYear(), filmLeft.getReleaseYear());
	}
	
	public static Comparator<Film> getFilmByRatingComparator()
	{
		return (filmLeft, filmRight) -> Integer.compare(filmRight.getRating(), filmLeft.getRating());
	}
	
	public static Comparator<Film> getFilmByRuntimeComparator()
	{
		return (filmLeft, filmRight) -> Integer.compare(filmRight.getRuntime(), filmLeft.getRuntime());
	}
	
	/**
	 * Pack film data and convert the attributes to a file format
	 * string.
	 * 
	 * @param film The film object to pack
	 * @return A string of the film object attributes
	 * @author Nikolai Kolbenev 15897074
	 */
	public static String packFilmData(Film film)
	{
		String data = 
				"Title=" + film.getTitle() +
				System.lineSeparator() + "Genre=" + film.getGenre() +
				System.lineSeparator() + "Director=" + film.getDirector() +
				System.lineSeparator() + "CastList=" + film.getCastList() + 
				System.lineSeparator() + "Rating=" + film.getRating() + 
				System.lineSeparator() + "ReleaseYear=" + film.getReleaseYear() + 
				System.lineSeparator() + "Runtime=" + film.getRuntime() + 
				System.lineSeparator() + FileManipulation.FILM_END_HARD_QUESSABLE_KEY;
		
		return data;
	}

	/**
	 * Unpack film data that was read from a file.
	 * 
	 * @param data The text data of film attributes
	 * @return The film object parsed from provided data
	 * @author Nikolai Kolbenev 15897074
	 */
	public static Film unpackFilmData(String data)
	{
		FilmTextData filmData = new FilmTextData();
		
		Scanner scan = new Scanner(data);
		while (scan.hasNext())
		{
			String[] attribute = scan.nextLine().trim().split("=", 2);
			if (attribute.length == 2)
			{
				attribute[0] = attribute[0].trim().toLowerCase();
				switch(attribute[0])
				{
				case "title":
					filmData.setTitle(attribute[1]);
					break;
				case "genre":
					filmData.setGenre(attribute[1]);
					break;
				case "director":
					filmData.setDirector(attribute[1]);
					break;
				case "castlist":
					filmData.setCastList(attribute[1]);
					break;
				case "rating":
					filmData.setRating(attribute[1]);
					break;
				case "releaseyear":
					filmData.setReleaseYear(attribute[1]);
					break;
				case "runtime":
					filmData.setRuntime(attribute[1].split(" ")[0]);
					break;
				default:
					break;
				}
			}
		}
		scan.close();
		return parseFilmDataFromText(filmData);
	}
	
	/**
	 * Parse film data and return the Film object if data
	 * is valid.
	 * 
	 * FilmTextData aggregates multiple attributes of 
	 * the real Film object, which are stored as string values.
	 * 
	 * @param filmData The FilmTextData object to be parsed
	 * @return The Film object parsed from text attributes of filmData, 
	 * if there was no success, return null
	 * @author Nikolai Kolbenev 15897074
	 */
	public static Film parseFilmDataFromText(FilmTextData filmData)
	{
		Film film = null;
		
		String invalidFields = getParseFilmDataResult(filmData);
		if (invalidFields.isEmpty())
		{
			try
			{			
				film = new Film(filmData);
			}
			catch(NumberFormatException ex)
			{
				System.err.println(ex + "\n" + ex.getMessage());
			};
		}
		
		return film;
	}
	
	/**
	 * Try parse the specified film data containing string values of Film attributes.
	 * The string returned contains feedback about which attributes were invalid and why.
	 * The filmData object usually represents the data obtained from GUI elements.
	 * 
	 * @param filmData The FilmTextData object
	 * @return A string containing all parsing and data conversion problems,
	 * an empty string if no problems were encountered
	 * @author Nikolai Kolbenev 15897074
	 */
	public static String getParseFilmDataResult(FilmTextData filmData)
	{
		String invalidFields = "";
		
		invalidFields += (filmData.getTitle().isEmpty() || filmData.getTitle() == null) ? "\"Title\" is missing\n" : "";
		invalidFields += (filmData.getDirector().isEmpty() || filmData.getDirector() == null) ? "\"Director\" is missing\n" : "";
		invalidFields += (filmData.getGenre().isEmpty() || filmData.getGenre() == null) ? "\"Genre\" is missing\n" : "";
		invalidFields += (filmData.getCastList().isEmpty() || filmData.getCastList() == null) ? "\"Cast List\" is missing\n" : "";
		
		try
		{
			invalidFields += (Integer.parseInt(filmData.getRating()) == -1) ? "\"Rating\" is not selected\n" : "";	
		}
		catch (NumberFormatException ex)
		{
			invalidFields += "\"Rating\" value must be an integer";	
		}
		
		try
		{
			invalidFields += (Integer.parseInt(filmData.getReleaseYear()) <= 0) ? "\"Release Year\" is invalid. (It shouldn't be!!!)\n" : "";	
		}
		catch (NumberFormatException ex)
		{
			invalidFields += "\"Release Year\" value must be an integer";	
		}
		
		try
		{
			invalidFields += (Integer.parseInt(filmData.getRuntime()) >= 0) ? "" : "\"Runtime\" value can't be negative\n";	
		}
		catch (NumberFormatException ex)
		{
			invalidFields += "\"Runtime\" value must be an integer";	
		}
		
		return invalidFields;
	}
}
