package application.utilities;

/**
 * The class provides storage for Film attributes in String values.
 * Useful when reading String data from GUI, no need to declare variables
 * because I have this class.
 *
 * @author Nikolai Kolbenev 15897074
 */
public class FilmTextData 
{
	private String title;
	private String genre;
	private String director;
	private String castList;
	private String rating;
	private String releaseYear;
	private String runtime;
	
	/**
	 * Get title String value
	 * 
	 * @return title String
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Set title String value
	 * 
	 * @param title String
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/** 
	 * Get genre String value
	 * 
	 * @return genre String
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getGenre() {
		return genre;
	}
	
	/**
	 * Set Genre String value
	 * 
	 * @param genre String
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	/**
	 * @return director String
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getDirector() {
		return director;
	}
	
	/**
	 * @param director String
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setDirector(String director) {
		this.director = director;
	}
	
	/**
	 * @return castList String
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getCastList() {
		return castList;
	}
	
	/**
	 * @param castList String
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setCastList(String castList) {
		this.castList = castList;
	}
	
	/**
	 * @return rating String
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getRating() {
		return rating;
	}
	
	/**
	 * @param rating String
	 * @author Nikolai Kolbenev 15897074
	 */ 
	public void setRating(String rating) {
		this.rating = rating;
	}
	
	/**
	 * @return releaseYear String
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getReleaseYear() {
		return releaseYear;
	}
	
	/**
	 * @param releaseYear String
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setReleaseYear(String releaseYear) {
		this.releaseYear = releaseYear;
	}
	
	/**
	 * @return runtime String
	 * @author Nikolai Kolbenev 15897074
	 */
	public String getRuntime() {
		return runtime;
	}
	
	/**
	 * @param runtime String
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}	
}
