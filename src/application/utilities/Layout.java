package application.utilities;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.LinearGradientPaint;
import java.awt.MultipleGradientPaint;
import java.awt.Point;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.border.StrokeBorder;
import javax.swing.border.TitledBorder;

/**
 * Layout class helps to define and follow common characteristics of GUI components.
 * It has functions that modify the style of borders of certain elements. 
 * These functions are accessible from the view menu items.
 *
 * @author Nikolai Kolbenev 15897074
 */
public final class Layout 
{
	public static final int indentFromLabelMarking = 8;
	public static final int indentFromPropertyBlock = 30;
	
	/**
	 * This applies to labels to adjust width and align them
	 * horizontally.
	 * 
	 * @param preferredSize The Dimension object reference which
	 * is a preferred size of some element
	 * @return The Dimension object with a new size 
	 * @author Nikolai Kolbenev 15897074
	 */
	public static Dimension getDefaultLabelSize(Dimension preferredSize)
	{
		return new Dimension(90, preferredSize.height);
	}
	
	/**
	 * This applies to text fields to adjust size and make
	 * them visually adjusted.
	 * 
	 * @return The Dimension object with a new size 
	 * @author Nikolai Kolbenev 15897074
	 */
	public static Dimension getDefaultTextFieldSize()
	{
		return new Dimension(240, 25);
	}
	
	/**
	 * This applies to runtime text fields.
	 * 
	 * @return The Dimension object with a new size 
	 * @author Nikolai Kolbenev 15897074
	 */
	public static Dimension getRuntimeFieldSize()
	{
		Dimension size = new Dimension(35, getDefaultTextFieldSize().height);
		return size;
	}
	
	/**
	 * Calculate position next to the component specified
	 * in arguments. This allows to keep similar distances 
	 * from jLabels across all views
	 * 
	 * @param component The relative component to measure position against
	 * @param indent The integer which is an indent from the relative component
	 * @return The new Point object with a new position
	 * @author Nikolai Kolbenev 15897074
	 */
	public static Point getPositionNextTo(JComponent component, int indent) 
	{
		Point position = new Point();

		position.x = component.getX() + component.getWidth() + indent;
		position.y = component.getY();

		return position;
	}

	/**
	 * Calculate position below the component specified
	 * in arguments. This allows to keep similar distances 
	 * between blocks of components (such as between the title and director
	 * label in the film view).
	 * 
	 * @param component The relative component to measure position against
	 * @param indent The integer which is an indent from the relative component
	 * @return The new Point object with a new position
	 * @author Nikolai Kolbenev 15897074
	 */
	public static Point getPositionBelow(JComponent component, int indent) 
	{
		Point position = new Point();

		position.x = component.getX();
		position.y = component.getY() + component.getHeight() + indent;
		
		return position;
	}
	
	/**
	 * The Text fields are usually bound to the corresponding labels (title label - title text field) so
	 * they need to be positioned nearby and be consistent in distance.
	 * 
	 * @param textField The JTextField component to apply the layout to
	 * @param relativeComponent The relative component against which the new location is measured 
	 * @author Nikolai Kolbenev 15897074
	 */
	public static void applyTextFieldLayout(JTextField textField, JComponent relativeComponent)
	{
		textField.setSize(Layout.getDefaultTextFieldSize());
		textField.setLocation(Layout.getPositionNextTo(relativeComponent, Layout.indentFromLabelMarking));
	}
	
	/**
	 * The labels sequentially follow each other (Title, Director, Genre...) and also 
	 * need to maintain consistent distances across the GUI.
	 * 
	 * @param label The JLabel component to apply the layout to
	 * @param relativeBlock The relative component against which the new location is measured 
	 * @author Nikolai Kolbenev 15897074
	 */
	public static void applyLabelLayout(JLabel label, JComponent relativeBlock)
	{
		label.setSize(Layout.getDefaultLabelSize(label.getPreferredSize()));
		
		if (relativeBlock != null)
		{
			label.setLocation(Layout.getPositionBelow(relativeBlock, Layout.indentFromPropertyBlock));
		}
	}

	/**
	 * Calculate center aligned location of the component relative
	 * to the another component. For correct results, the components
	 * must already have size values.
	 * 
	 * @param component The component to apply the new location
	 * @param relative The relative component to measure vertical center location against
	 * @return The point with coordinates of a new location
	 */
	public static Point getCentreAlignedLocation(JComponent component, JComponent relative, int indent)
	{
		Point location = new Point(Layout.getPositionNextTo(relative, indent).x, 
				relative.getY() - (component.getHeight() - relative.getHeight()) / 2);
		
		return location;
	}

	/**
	 * Apply the default border to each component in the specified array.
	 * This function is accessible from the view menu item.
	 * 
	 * @param components An array of JComponent objects to apply the border to
	 * @author Nikolai Kolbenev 15897074
	 */
	public static void applyDefaultBorder(JComponent[] components)
	{		
		for (JComponent component: components) 
		{
			component.setBorder(new TitledBorder(new StrokeBorder(new BasicStroke(1)), "", TitledBorder.CENTER, TitledBorder.TOP)); 
		}
	}

	/**
	 * Apply two color border to each component in the specified array.
	 * 
	 * @param components An array of JComponent objects to apply the border to
	 * @author Nikolai Kolbenev 15897074
	 */
	public static void applyTwoColorGradient(JComponent[] components)
	{
		for (JComponent component: components)
		{
			GradientPaint twoColorPaint = new GradientPaint(0.0f,  0.0f, Color.BLUE, component.getWidth()/4.0f, component.getHeight()/4.0f, Color.CYAN, true);
			
			component.setBorder(new TitledBorder(new StrokeBorder(new BasicStroke(5), twoColorPaint), 
					"", TitledBorder.CENTER, TitledBorder.TOP));
		}
	}

	/**
	 * Apply rainbow border to each component in the specified array.
	 * 
	 * @param components An array of JComponent objects to apply the border to
	 * @author Nikolai Kolbenev 15897074
	 */
	public static void applyRainbowGradient(JComponent[] components)
	{
		for (JComponent component: components)
		{
			Color[] colors = {Color.CYAN, Color.BLUE, Color.PINK, Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN};
			float[] fractions = new float[7];
			for (int i = 0; i < fractions.length; i++)
			{
				fractions[i] = i / (float)fractions.length;
			}

			MultipleGradientPaint rainbowPaint = new LinearGradientPaint(0.0f, 0.0f, component.getWidth(), component.getHeight(), fractions, colors);
			component.setBorder(new TitledBorder(new StrokeBorder(new BasicStroke(5), rainbowPaint), 
					"", TitledBorder.CENTER, TitledBorder.TOP));
		}
	}
}
