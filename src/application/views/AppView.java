package application.views;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.StrokeBorder;
import javax.swing.border.TitledBorder;

import application.models.Film;
import application.models.FilmDatabase;
import application.models.FilmReference;
import application.utilities.Layout;

/**
 * AppView brings all other views together into one place.
 * The location of some views components are dependent on components from other views,
 * this is where the interface layout gets corrected.
 *
 * @author Nikolai Kolbenev 15897074
 */
public class AppView extends JPanel
{
	private final String TITLE_FILM_SEARCH = "Film Search";
	private final String TITLE_FILM_DATABASE = "Film Database";
	private final String TITLE_FILM_DATA = "Film Description";
	
	private FilmDatabase filmDBModel;
	
	private FilmView filmPropertiesView;
	private FilmSearchView filmSearchView;
	
	private JTextArea areaNotification;
	private JLabel labelFilmSearch;
	private JLabel labelFilmData;
	private JLabel labelFilmDatabase;
	private JLabel labelSearchModeTip;
	private JComboBox<String> comboBoxFilmOrdering;
	private JList<Film> jListFilms;
	private JScrollPane scrollPaneForFilms;
	private JButton buttonSearchFilm;
	private JButton buttonShowAllFilms;
	private JButton buttonClearSearch;
	private JButton buttonAddFilm;
	private JButton buttonSaveModifiedFilm;
	private JButton buttonNewFilm;
	private JButton buttonRemoveFilm;
	
	private Timer timerColorAnimator;
	private TimerTask taskAnimateColor;
	private TimerTask taskAnimateColorReverse;
	
	/**
	 * Create a new AppView object
	 * 
	 * @param filmDB The reference to a film database
	 * @param filmView The FilmView object containing elements
	 * that allow to data of the film
	 * @param filmSearchView The FilmSearchView object that contains
	 * elements to enable input searching patterns
	 * @author Nikolai Kolbenev 15897074
	 */
	public AppView(FilmDatabase filmDB, FilmView filmView, FilmSearchView filmSearchView)
	{
		super();
		this.filmDBModel = filmDB;
		this.filmPropertiesView = filmView;
		this.filmSearchView = filmSearchView;
		this.timerColorAnimator = new Timer();
		this.setLayout(null);
		
		initComponents();
		loadComponents();
	}
	
	/**
	 * Initializes all components of this view
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	private void initComponents()
	{
		int viewDistanceFromLabel = 20;
		
		//Init Film Search Components
		labelFilmSearch = new JLabel(TITLE_FILM_SEARCH);
		labelFilmSearch.setSize(labelFilmSearch.getPreferredSize());
		labelFilmSearch.setLocation(0, 10);
		
		filmSearchView.setLocation(0, labelFilmSearch.getY() + labelFilmSearch.getHeight() + viewDistanceFromLabel);
		filmSearchView.setSize(390, 390);
		
		labelFilmSearch.setLocation(filmSearchView.getX() + filmSearchView.getWidth()/2 - labelFilmSearch.getWidth()/2, labelFilmSearch.getY());
		//============================
		
		//Init Database View Components
		labelFilmDatabase = new JLabel(TITLE_FILM_DATABASE);
		labelFilmDatabase.setSize(labelFilmDatabase.getPreferredSize());
		labelFilmDatabase.setLocation(0, labelFilmSearch.getY());
		
		jListFilms = new JList<Film>();
		jListFilms.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		scrollPaneForFilms = new JScrollPane(jListFilms);
		scrollPaneForFilms.setSize(300, 430);
		scrollPaneForFilms.setLocation(filmSearchView.getX() + filmSearchView.getWidth() + 50, labelFilmDatabase.getY() + labelFilmDatabase.getHeight() + viewDistanceFromLabel);
		
		labelFilmDatabase.setLocation(scrollPaneForFilms.getX() + scrollPaneForFilms.getWidth()/2 - labelFilmDatabase.getWidth()/2, labelFilmDatabase.getY());
		
		comboBoxFilmOrdering = new JComboBox<String>(new String[]{"By Title", "By Year", "By Rating", "By Runtime"});
		comboBoxFilmOrdering.setSize(scrollPaneForFilms.getX() + scrollPaneForFilms.getWidth() - labelFilmDatabase.getX() - 
				labelFilmDatabase.getWidth() - 2 * Layout.indentFromLabelMarking, comboBoxFilmOrdering.getPreferredSize().height);
		comboBoxFilmOrdering.setLocation(Layout.getCentreAlignedLocation(comboBoxFilmOrdering, labelFilmDatabase, 2 * Layout.indentFromLabelMarking));
		//=================================
		
		int buttonCommonWidth = 90;
		//Init Film Search Components 2 (Depends on Database View)
		buttonSearchFilm = new JButton("Search");
		buttonSearchFilm.setFocusPainted(false);
		buttonSearchFilm.setSize(buttonCommonWidth, buttonSearchFilm.getPreferredSize().height);
		buttonSearchFilm.setLocation(filmSearchView.getX() + (int)(filmSearchView.getWidth()/2.0 - 1.5 * buttonCommonWidth - Layout.indentFromPropertyBlock),
				scrollPaneForFilms.getY() + scrollPaneForFilms.getHeight() - buttonSearchFilm.getHeight());
		
		buttonShowAllFilms = new JButton("Show All");
		buttonShowAllFilms.setFocusPainted(false);
		buttonShowAllFilms.setSize(buttonCommonWidth, buttonSearchFilm.getSize().height);
		buttonShowAllFilms.setLocation(buttonSearchFilm.getX() + buttonSearchFilm.getWidth() + Layout.indentFromPropertyBlock, buttonSearchFilm.getY());
		
		buttonClearSearch = new JButton("Reset");
		buttonClearSearch.setFocusPainted(false);
		buttonClearSearch.setSize(buttonCommonWidth, buttonShowAllFilms.getSize().height);
		buttonClearSearch.setLocation(buttonShowAllFilms.getX() + buttonShowAllFilms.getWidth() + Layout.indentFromPropertyBlock, buttonShowAllFilms.getY());
		//============================
		
		buttonCommonWidth = 80;
		//Init Film Properties View Components
		filmPropertiesView.setLocation(scrollPaneForFilms.getX() + scrollPaneForFilms.getWidth() + 50, scrollPaneForFilms.getY());
		filmPropertiesView.setSize(390, 390);
		
		labelFilmData = new JLabel(TITLE_FILM_DATA);
		labelFilmData.setSize(labelFilmData.getPreferredSize());
		labelFilmData.setLocation(filmPropertiesView.getX() + filmPropertiesView.getWidth()/2 - labelFilmData.getWidth()/2, labelFilmDatabase.getY());

		buttonAddFilm = new JButton("Add");
		buttonAddFilm.setFocusPainted(false);
		buttonAddFilm.setSize(buttonCommonWidth, buttonAddFilm.getPreferredSize().height);
		buttonAddFilm.setLocation(filmPropertiesView.getX() + (int)(filmPropertiesView.getWidth()/2.0 - 1.5 * buttonCommonWidth - Layout.indentFromPropertyBlock), 
				scrollPaneForFilms.getY() + scrollPaneForFilms.getHeight() - buttonAddFilm.getHeight());

		buttonNewFilm = new JButton("New");
		buttonNewFilm.setFocusPainted(false);
		buttonNewFilm.setSize(buttonAddFilm.getSize());
		buttonNewFilm.setLocation(buttonAddFilm.getLocation());
		
		buttonSaveModifiedFilm = new JButton("Save");
		buttonSaveModifiedFilm.setFocusPainted(false);
		buttonSaveModifiedFilm.setSize(buttonCommonWidth, buttonSaveModifiedFilm.getPreferredSize().height);
		buttonSaveModifiedFilm.setLocation(buttonAddFilm.getX() + buttonAddFilm.getWidth() + Layout.indentFromPropertyBlock, buttonAddFilm.getY());
		
		buttonRemoveFilm = new JButton("Remove");
		buttonRemoveFilm.setFocusPainted(false);
		buttonRemoveFilm.setSize(buttonCommonWidth, buttonRemoveFilm.getPreferredSize().height);
		buttonRemoveFilm.setLocation(buttonSaveModifiedFilm.getX() + buttonSaveModifiedFilm.getWidth() + Layout.indentFromPropertyBlock, buttonSaveModifiedFilm.getY());
		//===================================================
		
		labelSearchModeTip = new JLabel("Click \"Show All\" button on the left to enable film editing...");
		labelSearchModeTip.setSize(labelSearchModeTip.getPreferredSize());
		labelSearchModeTip.setLocation(filmPropertiesView.getX() + (int)(filmPropertiesView.getWidth()/2.0 - labelSearchModeTip.getWidth()/2.0), 
				buttonAddFilm.getY());
		labelSearchModeTip.setVisible(false);

		areaNotification = new JTextArea();
		areaNotification.setEditable(false);
		areaNotification.setCursor(null);
		areaNotification.setFocusable(false);
		areaNotification.setSize(150, 80);
		areaNotification.setBackground(UIManager.getColor("Label.background"));
		areaNotification.setLineWrap(true);
		areaNotification.setWrapStyleWord(true);
		areaNotification.setLocation(Layout.getPositionNextTo(filmPropertiesView.getComboBoxReleaseYear(), Layout.indentFromPropertyBlock));
//		areaNotification.setBorder(new StrokeBorder(new BasicStroke(1), Color.BLACK));
		areaNotification.setBorder(new CompoundBorder(
				new StrokeBorder(new BasicStroke(1), Color.BLACK), 
				new EmptyBorder(3, 3, 3, 3)));
	}

	/**
	 * Adds all components to this view
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	private void loadComponents()
	{
		add(filmSearchView);
		add(labelFilmSearch);
		add(labelFilmDatabase);
		add(comboBoxFilmOrdering);
		add(labelFilmData);
		add(scrollPaneForFilms);
		add(filmPropertiesView);
		filmPropertiesView.add(areaNotification);
		add(buttonSearchFilm);
		add(buttonShowAllFilms);
		add(buttonClearSearch);
		add(buttonAddFilm);
		add(buttonNewFilm);
		add(buttonSaveModifiedFilm);
		add(buttonRemoveFilm);
		
		add(labelSearchModeTip);
	}
	
	/**
	 * Update components of this view whose states depend on the model objects.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void update()
	{
		updateButtonStates();
		this.updateListData();
	}
	
	/**
	 * Update the state of edit buttons to
	 * provide better user experience and ease of use.
	 * 
	 * The AddFilm button is only available when no film
	 * is currently selected. This makes it clear that the film attributes we set
	 * in GUI refer to the new film and not to an existing film in the list.
	 * 
	 * Likewise, the new button is displayed whenever an existing film is selected. 
	 * This separates film creation and film modification steps.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void updateButtonStates()
	{
		boolean isItemSelected = jListFilms.getSelectedIndices().length > 0;
		buttonSaveModifiedFilm.setEnabled(isItemSelected);
		buttonRemoveFilm.setEnabled(isItemSelected);

		setFilmViewButtonsVisibility(true);
		
		buttonAddFilm.setVisible(!isItemSelected);
		buttonNewFilm.setVisible(isItemSelected);
	}

	
	/**
	 * Updates the list and populates it with the films from the modified database.
	 */
	public void updateListData()
	{
		this.jListFilms.setListData(filmDBModel.toList());
	}
	
	/**
	 * This method is called when user enters search mode.
	 * In search mode, the list of films contain search results but not 
	 * the original database. So it doesn't make much sense to add to or remove
	 * from the search results any selected films. However, we should be able to view
	 * the film properties.
	 * 
	 * @param isVisible A boolean to set visibility of buttons
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setFilmViewButtonsVisibility(boolean isVisible)
	{
		this.buttonAddFilm.setVisible(isVisible);
		this.buttonNewFilm.setVisible(isVisible);
		this.buttonSaveModifiedFilm.setVisible(isVisible);
		this.buttonRemoveFilm.setVisible(isVisible);
	}
	
	/**
	 * @return The Dimension object which is the calculated size of this view
	 * @author Nikolai Kolbenev 15897074
	 */
	public Dimension calculatePreferredSize()
	{
		Dimension size = new Dimension(filmPropertiesView.getX() + filmPropertiesView.getWidth(), buttonRemoveFilm.getY() + buttonRemoveFilm.getHeight());
		return size;
	}

	/**
	 * Displays notification at the bottom right corner of the film view
	 * 
	 * @param message A String value to display
	 */
	public void displayNotification(String message)
	{	
		if (taskAnimateColor != null)
		{
			taskAnimateColor.cancel();
		}
		if (taskAnimateColorReverse != null)
		{
			taskAnimateColorReverse.cancel();
		}
		
		areaNotification.setText(message);
		areaNotification.setForeground(new Color(0, 0, 0, 0));

		taskAnimateColorReverse = new TimerTask() {
			@Override
			public void run() {
				Color color = new Color(0, 0, 255, Math.max(areaNotification.getForeground().getAlpha() - 5, 0));
				areaNotification.setForeground(color);
				areaNotification.repaint();

				if (areaNotification.getForeground().getAlpha() == 0)
				{
					taskAnimateColorReverse.cancel();
					taskAnimateColorReverse = null;
				}
			}
		};
		taskAnimateColor = new TimerTask() {
			@Override
			public void run() {
				Color color = new Color(0, 0, 255, Math.min(areaNotification.getForeground().getAlpha() + 5, 255));
				areaNotification.setForeground(color);
				areaNotification.repaint();

				if (areaNotification.getForeground().getAlpha() == 255)
				{
					timerColorAnimator.schedule(taskAnimateColorReverse, 2000, 20);
					taskAnimateColor.cancel();
					taskAnimateColor = null;
				}
			}
		};

		
		timerColorAnimator.schedule(taskAnimateColor, 0, 20);
	}

	/**
	 * @return the buttonAddFilm JButton
	 * @author Nikolai Kolbenev 15897074
	 */
	public JButton getButtonAddFilm() {
		return buttonAddFilm;
	}

	/**
	 * @return the buttonSaveModifiedFilm JButton
	 * @author Nikolai Kolbenev 15897074
	 */
	public JButton getButtonSaveModifiedFilm() {
		return buttonSaveModifiedFilm;
	}

	/**
	 * @return the buttonRemoveFilm JButton
	 * @author Nikolai Kolbenev 15897074
	 */
	public JButton getButtonRemoveFilm() {
		return buttonRemoveFilm;
	}

	/**
	 * @return the filmPropertiesView FilmView
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmView getFilmPropertiesView() {
		return filmPropertiesView;
	}

	/**
	 * @return the jListFilms JButton 
	 * @author Nikolai Kolbenev 15897074
	 */
	public JList<Film> getJListFilms() {
		return jListFilms;
	}

	/**
	 * @return the buttonNewFilm JButton 
	 * @author Nikolai Kolbenev 15897074
	 */
	public JButton getButtonNewFilm() {
		return buttonNewFilm;
	}

	/**
	 * @return the scrollPaneForFilms JScrollPane 
	 * @author Nikolai Kolbenev 15897074
	 */
	public JScrollPane getScrollPaneForFilms() {
		return scrollPaneForFilms;
	}
	
	/**
	 * @return the filmSearchView FilmSearchView 
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmSearchView getFilmSearchView() {
		return filmSearchView;
	}

	/**
	 * @return the buttonSearchFilm JButton 
	 * @author Nikolai Kolbenev 15897074
	 */
	public JButton getButtonSearchFilm() {
		return buttonSearchFilm;
	}

	/**
	 * @return the buttonShowAllFilms JButton 
	 * @author Nikolai Kolbenev 15897074
	 */
	public JButton getButtonShowAllFilms() {
		return buttonShowAllFilms;
	}

	/**
	 * @return the labelSearchModeTip JLabel 
	 * @author Nikolai Kolbenev 15897074
	 */
	public JLabel getLabelSearchModeTip() {
		return labelSearchModeTip;
	}

	/**
	 * @return the comboBoxFilmOrdering JComboBox
	 * @author Nikolai Kolbenev 15897074 
	 */
	public JComboBox<String> getComboBoxFilmOrdering() {
		return comboBoxFilmOrdering;
	}

	/**
	 * @return the buttonClearSearch object
	 * @author Nikolai Kolbenev 15897074 
	 */
	public JButton getButtonClearSearch() {
		return buttonClearSearch;
	}
}
