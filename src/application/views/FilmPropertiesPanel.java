package application.views;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import application.utilities.FilmDataManipulation;
import application.utilities.Layout;

/**
 * 
 * The FilmPropertiesPanel includes only those components that share common
 * layout (JLabel on the left + JTextField on the right), mainly the fields for
 * film title, director, genre and cast list.
 *
 * @author Nikolai Kolbenev 15897074
 */
public class FilmPropertiesPanel extends JPanel
{
	private JLabel labelTitle;
	private JLabel labelDirector;
	private JLabel labelGenre;
	private JLabel labelCastList;
	
	private JTextField textFieldTitle;
	private JTextField textFieldDirector;
	private JTextField textFieldGenre;
	private JTextArea textAreaCastList; //This class is used in both FilmView and FilmSearchView!
	private JScrollPane scrollPaneForCastList;
	
	/**
	 * create FilmMainPropertiesView object
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmPropertiesPanel()
	{
		this.setLayout(null);
		
		initComponents();
		loadComponents();
	}
	
	/**
	 * Initialize all components of this view
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void initComponents()
	{
		labelTitle = new JLabel("Title");
		labelTitle.setLocation(0, 0);
		Layout.applyLabelLayout(labelTitle, null);
		
		textFieldTitle = new JTextField();
		Layout.applyTextFieldLayout(textFieldTitle, labelTitle);
		
		labelDirector = new JLabel("Director");
		Layout.applyLabelLayout(labelDirector, labelTitle);
		
		textFieldDirector = new JTextField();
		Layout.applyTextFieldLayout(textFieldDirector, labelDirector);
		
		labelGenre = new JLabel("Genre");
		Layout.applyLabelLayout(labelGenre, labelDirector);
		
		textFieldGenre = new JTextField();
		Layout.applyTextFieldLayout(textFieldGenre, labelGenre);
		
		labelCastList = new JLabel("Cast List");
		Layout.applyLabelLayout(labelCastList, labelGenre);
		
		textAreaCastList = new JTextArea();
		textAreaCastList.setLineWrap(true);
		textAreaCastList.setWrapStyleWord(true);
		textAreaCastList.getDocument().putProperty("filterNewlines", Boolean.TRUE); //Disable new lines as they may corrupt the format of my film data saved in file
		
		scrollPaneForCastList = new JScrollPane (textAreaCastList, 
				   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPaneForCastList.setSize(Layout.getDefaultTextFieldSize().width, 80);
		scrollPaneForCastList.setLocation(Layout.getPositionNextTo(labelCastList, Layout.indentFromLabelMarking));
	}
	
	/**
	 * Loads all components of this view
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void loadComponents()
	{
		add(labelTitle);
		add(labelDirector);
		add(labelGenre);
		add(labelCastList);
		
		add(textFieldTitle);
		add(textFieldDirector);
		add(textFieldGenre);
		
		add(scrollPaneForCastList);
	}
	
	/**
	 * Clear fields and all data associated with film propeties.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void clearFields()
	{
		textFieldTitle.setText("");
		textFieldDirector.setText("");
		textFieldGenre.setText("");
		textAreaCastList.setText("");
	}
	
	/**
	 * @return The Dimension object which is the calculated size of this view
	 * @author Nikolai Kolbenev 15897074
	 */
	public Dimension calculatePreferredSize()
	{
		Dimension size = new Dimension(scrollPaneForCastList.getX() + scrollPaneForCastList.getWidth(), 
				scrollPaneForCastList.getY() + scrollPaneForCastList.getHeight());
		return size;
	}

	/**
	 * @return lableTitle JLabel
	 * @author Nikolai Kolbenev 15897074
	 */
	public JLabel getLabelTitle() {
		return labelTitle;
	}

	/**
	 * @return labelDirector JLabel
	 * @author Nikolai Kolbenev 15897074
	 */
	public JLabel getLabelDirector() {
		return labelDirector;
	}

	/**
	 * @return labelGenre JLabel
	 * @author Nikolai Kolbenev 15897074
	 */
	public JLabel getLabelGenre() {
		return labelGenre;
	}

	/**
	 * @return labelCastList JLabel
	 * @author Nikolai Kolbenev 15897074
	 */
	public JLabel getLabelCastList() {
		return labelCastList;
	}

	/**
	 * @return textFieldTitle JTextField
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextField getTextFieldTitle() {
		return textFieldTitle;
	}

	/**
	 * @return textFieldDirector JTextField
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextField getTextFieldDirector() {
		return textFieldDirector;
	}

	/**
	 * @return textFieldGenre JTextField
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextField getTextFieldGenre() {
		return textFieldGenre;
	}

	/**
	 * @return textAreaCastList JTextArea
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextComponent getTextAreaCastList() {
		return textAreaCastList;
	}

	/**
	 * @return scrollPaneForCastList JScrollPane
	 * @author Nikolai Kolbenev 15897074
	 */
	public JScrollPane getScrollPaneForCastList() {
		return scrollPaneForCastList;
	}
}
