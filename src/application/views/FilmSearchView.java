package application.views;


import java.awt.ScrollPane;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

import application.utilities.FilmDataManipulation;
import application.utilities.Layout;

/**
 * The film search view encapsulates components that contain
 * data about the films to search for or regular expressions to use when searching
 * for specific fields of a film (Title, Genre, Director, Cast List).
 *
 * Regular expressions are case sensitive by default. Insensitive case can be set with a flag (?i). 
 * This would be quite useful in a Title field because the title itself is case insensitive.
 * 
 * @author Nikolai Kolbenev 15897074
 */
public class FilmSearchView extends JPanel
{
	private FilmPropertiesPanel filmMainPropView;
	
	private JLabel labelRaiting;
	private JLabel labelYear;
	private JLabel labelRuntime;
	private JLabel labelFromToRating;
	private JLabel labelFromToYear;
	private JLabel labelFromToRuntime;
	private JLabel labelRegex;
	
	private JCheckBox checkBoxRegex;
	
	private JComboBox<Integer> comboBoxFromRating;
	private JComboBox<Integer> comboBoxToRating;
	private JComboBox<Integer> comboBoxFromYear;
	private JComboBox<Integer> comboBoxToYear;
	
	private JTextField textFieldFromRuntime;
	private JTextField textFieldToRuntime;
	
	/**
	 * Create a FilmSearchView object
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmSearchView()
	{
		this.setLayout(null);
		
		initComponents();
		loadComponents();
	}
	
	/**
	 * Initialize components of this view.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void initComponents()
	{
		filmMainPropView = new FilmPropertiesPanel();
		filmMainPropView.getScrollPaneForCastList().setSize(Layout.getDefaultTextFieldSize());
		filmMainPropView.setSize(filmMainPropView.calculatePreferredSize());
		filmMainPropView.setLocation(20, 25);
		filmMainPropView.getScrollPaneForCastList().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);

		//Rating elements
		labelRaiting = new JLabel("Rating");
		labelRaiting.setSize(Layout.getDefaultLabelSize(labelRaiting.getPreferredSize()));
		labelRaiting.setLocation(Layout.getPositionBelow(filmMainPropView, Layout.indentFromPropertyBlock - 6));
		
		comboBoxFromRating = new JComboBox<Integer>(new Integer[] { 1, 2, 3, 4, 5 });
		comboBoxFromRating.setLocation(Layout.getPositionNextTo(labelRaiting, Layout.indentFromLabelMarking));
		comboBoxFromRating.setSize(comboBoxFromRating.getPreferredSize().width + 10, comboBoxFromRating.getPreferredSize().height);
		
		labelFromToRating = new JLabel("to");
		labelFromToRating.setSize(labelFromToRating.getPreferredSize());

		comboBoxToRating = new JComboBox<Integer>(new Integer[] { 1, 2, 3, 4, 5 });
		comboBoxToRating.setSize(comboBoxFromRating.getPreferredSize().width + 10, comboBoxFromRating.getPreferredSize().height);
		comboBoxToRating.setSelectedIndex(comboBoxToRating.getItemCount() - 1);
		
		//Year elements
		labelYear = new JLabel("Year");
		labelYear.setSize(Layout.getDefaultLabelSize(labelYear.getPreferredSize()));
		labelYear.setLocation(Layout.getPositionBelow(labelRaiting, Layout.indentFromPropertyBlock));
		
		comboBoxFromYear = new JComboBox<Integer>(FilmDataManipulation.getReleaseYearValues());
		comboBoxFromYear.setLocation(Layout.getPositionNextTo(labelYear, Layout.indentFromLabelMarking));
		comboBoxFromYear.setSize(comboBoxFromYear.getPreferredSize().width + 10, comboBoxFromRating.getPreferredSize().height);
		comboBoxFromYear.setSelectedIndex(comboBoxFromYear.getItemCount() - 1);
		
		//Rating updated elements whose locations depend on year elements above
		comboBoxFromRating.setLocation(comboBoxFromRating.getX() + comboBoxFromYear.getWidth() - comboBoxFromRating.getWidth(), comboBoxFromRating.getY());
		labelFromToRating.setLocation(Layout.getCentreAlignedLocation(labelFromToRating, comboBoxFromRating, Layout.indentFromLabelMarking));
		comboBoxToRating.setLocation(Layout.getCentreAlignedLocation(comboBoxToRating, labelFromToRating, Layout.indentFromLabelMarking));
		
		//Year updated elements whose locations depend on rating elements above
		labelFromToYear = new JLabel("to");
		labelFromToYear.setSize(labelFromToRating.getPreferredSize());
		labelFromToYear.setLocation(Layout.getCentreAlignedLocation(labelFromToYear, comboBoxFromYear, Layout.indentFromLabelMarking));
		
		comboBoxToYear = new JComboBox<Integer>(FilmDataManipulation.getReleaseYearValues());
		comboBoxToYear.setSize(comboBoxFromYear.getPreferredSize().width + 10, comboBoxFromRating.getPreferredSize().height);
		comboBoxToYear.setLocation(Layout.getCentreAlignedLocation(comboBoxToYear, labelFromToYear, Layout.indentFromLabelMarking));
		comboBoxToYear.setSelectedIndex(0);
		
		//Runtime elements
		labelRuntime = new JLabel("Runtime (mins)");
		labelRuntime.setSize(Layout.getDefaultLabelSize(labelRuntime.getPreferredSize()));
		labelRuntime.setLocation(Layout.getPositionBelow(labelYear, Layout.indentFromPropertyBlock));
		
		textFieldFromRuntime = new JTextField("0");
		textFieldFromRuntime.setSize(Layout.getRuntimeFieldSize());
		textFieldFromRuntime.setLocation(Layout.getPositionNextTo(labelRuntime, Layout.indentFromLabelMarking));
		textFieldFromRuntime.setLocation(textFieldFromRuntime.getLocation().x + comboBoxFromYear.getWidth() - textFieldFromRuntime.getWidth(), textFieldFromRuntime.getLocation().y);
		
		labelFromToRuntime = new JLabel("to");
		labelFromToRuntime.setSize(labelFromToRuntime.getPreferredSize());
		labelFromToRuntime.setLocation(Layout.getCentreAlignedLocation(labelFromToRuntime, textFieldFromRuntime, Layout.indentFromLabelMarking));
		
		textFieldToRuntime = new JTextField("250");
		textFieldToRuntime.setSize(Layout.getRuntimeFieldSize());
		textFieldToRuntime.setLocation(Layout.getCentreAlignedLocation(textFieldToRuntime, labelFromToRuntime, Layout.indentFromLabelMarking));
		
		//Regex elements
		labelRegex = new JLabel("Use regex search patterns (case sensitive!)"); //Regex is case sensitive by default, however you can make it insensitive with a flag (?i)
		labelRegex.setSize(labelRegex.getPreferredSize());
		labelRegex.setLocation(labelRuntime.getX(), labelRuntime.getY() + labelRuntime.getHeight() + 40);
		
		checkBoxRegex = new JCheckBox();
		checkBoxRegex.setSize(checkBoxRegex.getPreferredSize());
		checkBoxRegex.setLocation(Layout.getCentreAlignedLocation(checkBoxRegex, labelRegex, Layout.indentFromLabelMarking));
	}

	/**
	 * Add elements to this view
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void loadComponents()
	{
		add(filmMainPropView);
		
		add(labelRaiting);
		add(labelYear);
		add(labelRuntime);
		add(labelFromToRating);
		add(labelFromToYear);
		add(labelFromToRuntime);
		add(labelRegex);
		
		add(checkBoxRegex);
		
		add(comboBoxFromRating);
		add(comboBoxToRating);
		add(comboBoxFromYear);
		add(comboBoxToYear);
		add(textFieldFromRuntime);
		add(textFieldToRuntime);
	}
	
	/**
	 * Clear fields and all data associated with the search patterns.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void clearFields()
	{
		filmMainPropView.clearFields();
		comboBoxFromRating.setSelectedIndex(0);
		comboBoxToRating.setSelectedIndex(comboBoxToRating.getItemCount() - 1);
		comboBoxToYear.setSelectedIndex(0);
		comboBoxFromYear.setSelectedIndex(comboBoxToYear.getItemCount() - 1);
		textFieldFromRuntime.setText("0");
		textFieldToRuntime.setText("250");
	}

	/**
	 * @return comboBoxFromRating which is a JComboBox
	 * @author Nikolai Kolbenev 15897074
	 */
	public JComboBox<Integer> getComboBoxFromRating() {
		return comboBoxFromRating;
	}

	/**
	 * @return comboBoxToRating which is a JComboBox
	 * @author Nikolai Kolbenev 15897074
	 */
	public JComboBox<Integer> getComboBoxToRating() {
		return comboBoxToRating;
	}

	/**
	 * @return comboBoxFromYear which is a JComboBox
	 * @author Nikolai Kolbenev 15897074
	 */
	public JComboBox<Integer> getComboBoxFromYear() {
		return comboBoxFromYear;
	}

	/**
	 * @return comboBoxToYear which is a JComboBox
	 * @author Nikolai Kolbenev 15897074
	 */
	public JComboBox<Integer> getComboBoxToYear() {
		return comboBoxToYear;
	}

	/**
	 * @return textFieldFromRuntime which is a JTextField
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextField getTextFieldFromRuntime() {
		return textFieldFromRuntime;
	}

	/**
	 * @return textFieldToRuntime which is a JTextField
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextField getTextFieldToRuntime() {
		return textFieldToRuntime;
	}
	
	/**
	 * textFieldTitle is stored in the filmMainPropView. 
	 * The FilmPropertiesPanel view is a template for components with common layout.
	 * 
	 * @return TextFieldTitle object which is a JTextField
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextField getTextFieldTitle()
	{
		return filmMainPropView.getTextFieldTitle();
	}
	
	/**
	 * @return TextFieldDirector object which is a JTextField
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextField getTextFieldDirector()
	{
		return filmMainPropView.getTextFieldDirector();
	}
	
	/**
	 * @return TextFieldGenre object which is a JTextField
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextField getTextFieldGenre()
	{
		return filmMainPropView.getTextFieldGenre();
	}
	
	/**
	 * @return TextAreaCastList object which is a JTextArea
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextComponent getTextAreaCastList()
	{
		return filmMainPropView.getTextAreaCastList();
	}

	/**
	 * @return checkBoxRegex which is a JCheckBox
	 * @author Nikolai Kolbenev 15897074
	 */
	public JCheckBox getCheckBoxRegex() {
		return checkBoxRegex;
	}
}
