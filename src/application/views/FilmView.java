package application.views;

import java.time.LocalDate;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import application.models.FilmReference;
import application.utilities.FilmDataManipulation;
import application.utilities.Layout;

/**
 * The FilmView class contains components needed to enter,
 * store and display the data about a particular film such as its cast list, rating, release year,
 * runtime and other.
 *
 * @author Nikolai Kolbenev 15897074
 */
public class FilmView extends JPanel
{
	private FilmReference selectedFilmReference;
	
	private FilmPropertiesPanel filmMainPropView;
	
	private JRadioButton[] ratingRadioButtons;
	private JLabel labelRating;
	private JLabel labelReleaseYear;
	private JLabel labelRuntime;
	private JTextField textFieldRuntime;
	private JComboBox<Integer> comboBoxReleaseYear;
	
	/**
	 * Create an object of FilmView, initialized with a filmReference.
	 * The FilmReference class itself contains a reference to
	 * the real film object, This allows to have a common reference (FilmReference) pointing to 
	 * the same location any time a new film is selected (film variable in FilmReference).
	 * 
	 * @param filmReference The FilmReference object for storing the currently selected film in the list
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmView(FilmReference filmReference)
	{
		this.selectedFilmReference = filmReference;
		this.setLayout(null);

		initComponents();
		loadComponents();
	}

	/**
	 * Initialize all components of this view
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	private void initComponents()
	{
		filmMainPropView = new FilmPropertiesPanel();
		filmMainPropView.setLocation(20, 25);
		filmMainPropView.setSize(filmMainPropView.calculatePreferredSize());
		
		labelRating = new JLabel("Rating");
		labelRating.setSize(Layout.getDefaultLabelSize(labelRating.getPreferredSize()));
		//labelRating.setLocation(labelCastList.getX(), scrollPaneForCastList.getY() + scrollPaneForCastList.getHeight() + Layout.indentFromLabelMarking);
		labelRating.setLocation(filmMainPropView.getX(), filmMainPropView.getY() + filmMainPropView.getHeight() + Layout.indentFromLabelMarking);
		
		//Initialize radio buttons
		ratingRadioButtons = new JRadioButton[5];
		for (int rating = 0; rating < ratingRadioButtons.length; rating++)
		{
			ratingRadioButtons[rating] = new JRadioButton(String.valueOf(rating + 1));
			if (rating == 0)
			{
				ratingRadioButtons[rating].setLocation(
						Layout.getPositionNextTo(labelRating, Layout.indentFromLabelMarking));
			}
			else
			{
				ratingRadioButtons[rating].setLocation(
						Layout.getPositionNextTo(ratingRadioButtons[rating - 1], Layout.indentFromLabelMarking));
			}
			ratingRadioButtons[rating].setFocusPainted(false);
			ratingRadioButtons[rating].setSize(ratingRadioButtons[rating].getPreferredSize());
			
			add(ratingRadioButtons[rating]);
		}
		
		labelReleaseYear = new JLabel("Release Year");
		Layout.applyLabelLayout(labelReleaseYear, labelRating);
		
		comboBoxReleaseYear = new JComboBox<Integer>(FilmDataManipulation.getReleaseYearValues());
		comboBoxReleaseYear.setLocation(labelReleaseYear.getX() + labelReleaseYear.getWidth() + Layout.indentFromLabelMarking, labelReleaseYear.getY() - 3);
		comboBoxReleaseYear.setSize(comboBoxReleaseYear.getPreferredSize().width + 20, comboBoxReleaseYear.getPreferredSize().height);
		((JLabel)comboBoxReleaseYear.getRenderer()).setHorizontalAlignment(JLabel.CENTER);
		
		labelRuntime = new JLabel("Runtime (mins)");
		Layout.applyLabelLayout(labelRuntime, labelReleaseYear);
		
		textFieldRuntime = new JTextField();
		textFieldRuntime.setSize(Layout.getRuntimeFieldSize());
		textFieldRuntime.setLocation(Layout.getCentreAlignedLocation(textFieldRuntime, labelRuntime, Layout.indentFromLabelMarking));
	}
	
	/**
	 * Add components to this view
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	private void loadComponents()
	{
		add(filmMainPropView);
		add(labelRating);
		add(labelReleaseYear);
		add(labelRuntime);
		add(textFieldRuntime);
		add(comboBoxReleaseYear);
	}
	
	/**
	 * Clear fields and all data associated with the previously selected film. This
	 * provides visual aid, meaning that a new film is going to be created.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void clearFields()
	{
		filmMainPropView.clearFields();
		textFieldRuntime.setText("");
		setSelectedRating(-1);
		comboBoxReleaseYear.setSelectedIndex(FilmDataManipulation.MAX_RELEASE_YEAR - FilmDataManipulation.MIN_RELEASE_YEAR);
	}
	
	/**
	 * @return An integer corresponding to the rating of a film
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public int getSelectedRating()
	{
		for (JRadioButton rButton: ratingRadioButtons)
		{
			if (rButton.isSelected())
			{
				return Integer.parseInt(rButton.getText());
			}
		}

		return -1;
	}
	
	/**
	 * Selects a radio button that matches the rating passed as an argument,
	 * otherwise removes selection from all buttons.
	 * 
	 * @param rating An integer as a film rating
	 * @author Nikolai Kolbenev 15897074
	 */
	public void setSelectedRating(int rating)
	{
		for (JRadioButton rButton: ratingRadioButtons)
		{
			rButton.setSelected(false);
		}
		
		if (rating > 0 && rating <= ratingRadioButtons.length)
		{
			ratingRadioButtons[rating - 1].setSelected(true);
		}
	}

	/**
	 * Populates the UI fields with data about currently selected film, or
	 * empty values if no film is selected. The film reference is the model
	 * of this view.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public void update() 
	{
		String title = "";
		String director = "";
		String genre = "";
		String castList = "";
		int rating = -1;
		int releaseYear = LocalDate.now().getYear();
		String runtime = "";
		
		if (selectedFilmReference != null && selectedFilmReference.getFilm() != null)
		{
			title = selectedFilmReference.getFilm().getTitle();
			director = selectedFilmReference.getFilm().getDirector();
			genre = selectedFilmReference.getFilm().getGenre();
			castList = selectedFilmReference.getFilm().getCastList();
			rating = selectedFilmReference.getFilm().getRating();
			releaseYear = selectedFilmReference.getFilm().getReleaseYear();
			runtime = String.valueOf(selectedFilmReference.getFilm().getRuntime());
		}
		
		filmMainPropView.getTextFieldTitle().setText(title);
		filmMainPropView.getTextFieldDirector().setText(director);
		filmMainPropView.getTextFieldGenre().setText(genre);
		filmMainPropView.getTextAreaCastList().setText(castList);
		textFieldRuntime.setText(runtime);
		setSelectedRating(rating);
		if (releaseYear >= FilmDataManipulation.MIN_RELEASE_YEAR && releaseYear <= FilmDataManipulation.MAX_RELEASE_YEAR)
		{
			comboBoxReleaseYear.setSelectedIndex(FilmDataManipulation.MAX_RELEASE_YEAR - releaseYear);
		}
	}
	
	/**
	 * @return The ratingRadioButtons which is a JRadioButton
	 * @author Nikolai Kolbenev 15897074
	 */
	public JRadioButton[] getRatingRadioButtons() {
		return ratingRadioButtons;
	}
	
	/**
	 * @return The comboBoxReleaseYear which is a JComboBox
	 * @author Nikolai Kolbenev 15897074
	 */
	public JComboBox<Integer> getComboBoxReleaseYear() {
		return comboBoxReleaseYear;
	}

	/**
	 * @return The textFieldRuntime which is a JTextField
	 * @author Nikolai Kolbenev 15897074
	 */
	public JTextField getTextFieldRuntime() {
		return textFieldRuntime;
	}

	/**
	 * @return The filmMainPropView which is a FilmPropertiesPanel
	 * @author Nikolai Kolbenev 15897074
	 */
	public FilmPropertiesPanel getFilmMainPropView() {
		return filmMainPropView;
	}
}
