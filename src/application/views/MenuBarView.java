package application.views;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * The menu bar view for this application containing elements
 * that are hard to place on the window but that are necessary
 * for certain functionality (Saving, saving as, opening the database and
 * changing the view style).
 * 
 * Only menu itmes are accessible from outside of this class.
 * @author Nikolai Kolbenev 15897074
 */
public class MenuBarView extends JMenuBar
{
	private JMenu menuFile;
	private JMenuItem menuItemOpenDB;
	private JMenuItem menuItemSaveDB;
	private JMenuItem menuItemSaveAsDB;
	
	private JMenu menuView;
	private JMenu submenuChangeBorder;
	private JMenuItem menuItemRainbowBorder;
	private JMenuItem menuItemTwoColorBorder;
	private JMenuItem menuItemDefaultBorder;
	
	/**
	 * Create a new instance of MenuBarView
	 */
	public MenuBarView()
	{
		initComponents();
		loadComponents();
	}
	
	/**
	 * Initialize components of this menu bar
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	private void initComponents()
	{
		menuFile = new JMenu("File");
		menuItemOpenDB = new JMenuItem("Open database...");
		menuItemSaveDB = new JMenuItem("Save database");
		menuItemSaveAsDB = new JMenuItem("Save database as...");
		
		menuView = new JMenu("View");
		submenuChangeBorder = new JMenu("Film border view");
		menuItemRainbowBorder = new JMenuItem("Rainbow");
		menuItemTwoColorBorder = new JMenuItem("Two color");
		menuItemDefaultBorder = new JMenuItem("Default");
	}
	
	/**
	 * Add menus to this menu bar and add menu items to corresponding sub menus
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	private void loadComponents()
	{
		menuFile.add(menuItemOpenDB);
		menuFile.addSeparator();
		menuFile.add(menuItemSaveDB);
		menuFile.add(menuItemSaveAsDB);
		
		menuView.add(submenuChangeBorder);
		submenuChangeBorder.add(menuItemRainbowBorder);
		submenuChangeBorder.add(menuItemTwoColorBorder);
		submenuChangeBorder.add(menuItemDefaultBorder);
		
		this.add(menuFile);
		this.add(menuView);
	}

	/**
	 * @return The menuItemOpenDB which is a JMenuItem
	 * @author Nikolai Kolbenev 15897074
	 */
	public JMenuItem getMenuItemOpenDB() {
		return menuItemOpenDB;
	}

	/**
	 * @return The menuItemSaveAsDB which is a JMenuItem
	 * @author Nikolai Kolbenev 15897074
	 */
	public JMenuItem getMenuItemSaveAsDB() {
		return menuItemSaveAsDB;
	}

	/**
	 * @return The menuItemRainbowBorder which is a JMenuItem
	 * @author Nikolai Kolbenev 15897074
	 */
	public JMenuItem getMenuItemRainbowBorder() {
		return menuItemRainbowBorder;
	}

	/**
	 * @return The menuItemTwoColorBorder which is a JMenuItem
	 * @author Nikolai Kolbenev 15897074
	 */
	public JMenuItem getMenuItemTwoColorBorder() {
		return menuItemTwoColorBorder;
	}

	/**
	 * @return The menuItemDefaultBorder which is a JMenuItem
	 * @author Nikolai Kolbenev 15897074
	 */
	public JMenuItem getMenuItemDefaultBorder() {
		return menuItemDefaultBorder;
	}

	/**
	 * @return The menuItemSaveDB which is a JMenuItem
	 * @author Nikolai Kolbenev 15897074
	 */
	public JMenuItem getMenuItemSaveDB() {
		return menuItemSaveDB;
	}
}