package application.views;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

/**
 * The StatusView class that contains a single jLabel
 * to display the path to the database that was imported.
 *
 * @author Nikolai Kolbenev 15897074
 */
public class StatusView extends JPanel
{
	private JLabel labelFilePath;
	
	/**
	 * Create StatusView object. Align the view along the whole
	 * width of a parent window.
	 * 
	 * @author Nikolai Kolbenev 15897074
	 */
	public StatusView()
	{
		this.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		labelFilePath = new JLabel();
		labelFilePath.setHorizontalAlignment(SwingConstants.LEFT);
		
		this.add(labelFilePath);
	}
	
	/**
	 * @return The labelFilePath which is a JLabel
	 * @author Nikolai Kolbenev 15897074
	 */
	public JLabel getLabelFilePath()
	{
		return labelFilePath;
	}
}
